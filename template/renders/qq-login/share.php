<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-10 12:13
 *
 * 项目：levs  -  $  - share.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');
?>


<div class="page" id="login">
    <div class="page-content appbg" style="position: relative !important;padding: 10px 0;">
        <div class="page-content-inner" style="max-width:700px;">

            <div class="login-formm">
                <?php \modules\qq\widgets\qqlogin\qqLoginWidget::run($show, $qq, null, $logintype) ?>
            </div>

        </div>
    </div>
</div>

<script>
    jQuery(function () {
        qqJs.sharesu.suid = '<?=$suid?>';
        qqJs.sharesu.state = '<?=$state?>';
        qqJs.sharesu.sendCookieUrl = '<?=$mySendCookieUrl?>';
    });
    jQuery(function () {
        parent.jQuery('#dataloading').hide();
    });
</script>