<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-09 00:06
 *
 * 项目：levs  -  $  - index.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');

use modules\qq\helpers\UrlQqHelper;

?>

<div class="page" id="login">
    <div class="error_tips" id="error_tips" style="display: none;"><span class="error_logo" id="error_logo"></span>
        <span class="err_m" id="err_m"></span></div>

    <div class="qlogin_list" id="qlogin_list" style="width: 300px;"></div>

    <div class="page-content appbg" style="position: relative !important;">
        <div class="page-content-inner" style="max-width:700px;">

            <div class="login-formm">
                <?php \modules\qq\widgets\qqlogin\qqLoginWidget::run($show, $qq, null, $logintype) ?>
            </div>

            <div class="card">
                <div class="card-header">
                    我的QQ号
                </div>
                <div class="card-content-inner data-xtable">
                    <table>
                        <tr>
                            <th>状态</th>
                            <th>QQ号</th>
                            <th>昵称</th>
                            <th>好友数量</th>
                            <th class="numeric-cell">登陆时间</th>
                            <th>操作</th>
                        </tr>
                        <?php foreach ($qqs as $v): $sc = \modules\qq\helpers\qqLoginHelper::getQqdatas($v['qq']);?>
                        <tr class="qq-<?=$v['qq']?>">
                            <td>
                                <qqstatus><?=\modules\qq\table\qq\qqModelHelper::qqstatusCheckHtm($v['qqstatus'], $v['uptime'])?></qqstatus>
                            </td>
                            <td>
                                <a class="editField" opname="signmsg" opval="" href="<?=Lev::toReRoute(['qzone/edit-sign-and-dongtai', 'qq'=>$v['qq']])?>" title="发布空间动态"><svg class="icon"><use xlink:href="#fa-compose"></use></svg></a>
                                <?=$v['qq']?>
                            </td>
                            <td><a class="editField" opname="nick" opval="<?=$v['qqnick']?>" href="<?=Lev::toReRoute(['qzone/edit-nick', 'qq'=>$v['qq'], 'nick'=>$v['qqnick'], 'doit'=>1])?>"><svg class="icon"><use xlink:href="#fa-compose"></use></svg></a><nick><?=$v['qqnick']?></nick></td>
                            <td>
                                <a class="ajaxBtn" href="<?=UrlQqHelper::myfriend($v['qq'], 1)?>"><svg class="icon"><use xlink:href="#fa-refresh"></use></svg></a>
                                <hynum><?=Lev::arrv('hynum', $sc, 0)?></hynum>
                            </td>
                            <td class="numeric-cell"><p class="date"><?=Lev::asRealTime($v['uptime'])?></p></td>
                            <td>
                                <div class="buttons-row">
                                    <div class="flex-box scale8 transl">
                                    <a class="button button-small openPP" title="<?=$v['qqnick'],$v['qq']?> 的好友" href="<?=UrlQqHelper::myfriend($v['qq'])?>"><svg class="icon"><use xlink:href="#fa-qq"></use></svg></a>
                                        <a class="button button-small editField" opname="addqq" opval="" title="<?=$v['qqnick'],$v['qq']?> 添加好友" href="<?=UrlQqHelper::addfriend($v['qq'])?>"><svg class="icon"><use xlink:href="#fa-add"></use></svg></a>
                                        <a class="button button-small openPP" title="<?=$v['qqnick'],$v['qq']?> 的qq群" href="<?=UrlQqHelper::myqqgroup($v['qq'])?>">群</a>
                                        <a class="button button-small editField" opname="sendmsg" opval="123123||送个礼物" title="送礼物<p><tips>格式：qq号||送礼留言</tips>" href="<?=UrlQqHelper::sendgift($v['qq'], '')?>"><svg class="icon"><use xlink:href="#fa-fudai"></use></svg></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <form id="saveForm" autocomplete="off" action="<?=UrlQqHelper::sendQqUrl()?>" method="post" style="width: 100%;">
                        <div class="card-content ju-sa flex-box item-input">
                            <input type="text" name="qqurl" style="width:100%" class="form-control sendCookie" title="输入一个腾讯网址，验证登陆，必须以http开头" placeholder="输入一个腾讯网址，验证登陆，必须以http开头">
                            <input type="text" name="postpm" class="wd60" placeholder="POST参数，留空，以GET方式提交。例：&a=1&b=2&c=3" title="POST参数，留空，以GET方式提交。例：&a=1&b=2&c=3"/>
                            <input type="text" name="qq" class="wd60" placeholder="验证QQ" title="验证QQ"/>
                            <select name="gbk" title="返回数据编码" style="font-size: 12px">
                                <option value="0">UTF8</option>
                                <option value="1">GBK</option>
                            </select>
                            <a class="button button-fill color-orange dosaveFormBtn wdmin">执行</a>

                        </div>
                    </form>
                </div>
                <div class="card-content-inner ju-sa flex-box item-input">
                <textarea class="loginCookie form-control" title="成功登陆cookie" placeholder="成功登陆cookie" style="width:calc(100% - 40px); height:160px;font-size: 12px;color: #555;"></textarea>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="font14">传送cookie</div>
                    <span class="inblk font12 color-orange transr">登陆成功后以GET方式发送到 【接收cookie地址】。<br>接收方式：$_GET['qqcookies']、$_GET['qq']、$_GET['state']</span>
                </div>
                <form id="saveForm" autocomplete="off" action="<?=UrlQqHelper::saveSendCookieUrl()?>" method="post">
                <div class="card-content-inner ju-sa flex-box item-input">
                    <input type="text" name="link" style="width:100%" class="form-control sendCookie" title="接收cookie地址，必须以http开头" placeholder="接收cookie地址，必须以http开头" value="<?=$mySendCookieUrl?>">
                    <a class="button button-fill color-orange dosaveFormBtn wdmin">保存</a>

                </div>
                </form>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="font14">参数说明：</div>
                    <div class="buttons-row">
                        <a class="button-fill button button-small color-yellow" target="_blank" _bk="1" href="<?=UrlQqHelper::share()?>">预览</a>
                        <a class="button-fill button button-small color-lightblue copyBtn" copy-txt="<?=UrlQqHelper::share()?>">复制调用地址</a>
                    </div>
                </div>
                <div class="card-content-inner data-xtable">
                    <table style="width: 100%">
                        <tr>
                            <th>参数名</th>
                            <th>说明</th>
                        </tr>
                        <tr>
                            <td>qqcokies</td>
                            <td><p><tips>QQ成功登陆时返回的cookie，可携带此cookie用于其它操作</tips></p>
                                使用base64可传输加密。加密、解密PHP函数：
                                <div class="font12" style="position: relative">
                                <textarea class="copy-phpcode" style="color: #fff;background: black;width: 100%;font-size: 12px;height:160px;border: 1px solid gray;">
public static function base64_encode_url($string) {
    return str_replace(['+','/','='], ['-','_',''], base64_encode(($string)));
}

public static function base64_decode_url($string) {
    return (base64_decode(str_replace(['-','_'], ['+','/'], $string)));
}

                                </textarea>
                                    <a class="button-small button button-fill color-gray copyBtn" copy-input=".copy-phpcode" style="position: absolute;right: 10px;bottom: 10px;">复制</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>qq</td>
                            <td>登陆成功的QQ号</td>
                        </tr>
                        <tr>
                            <td>suid</td>
                            <td>您站点的用户UID仅限数字。<p><tips>自动从调用地址中获取并回传</tips></p></td>
                        </tr>
                        <tr>
                            <td>state</td>
                            <td>固定不变回传参数，例如：你站点的用户UID等<p><tips>自动从调用地址中获取并回传</tips></p></td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <?php Lev::footer(); ?>
    </div>

    <?php Lev::navbar();Lev::toolbar(); ?>
</div>
