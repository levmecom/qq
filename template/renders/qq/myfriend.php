<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 11:34
 *
 * 项目：levs  -  $  - myfriend.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');

$groupids = implode(', ', Lev::getArrayColumn($groups, ['groupid']));
?>

<div class="page myFriendPage">

    <div class="page-content">
        <div class="page-content-inner">
            <div class="data-xtable">
                <table>
                    <tr>
                        <th class="wd30">序号</th>
                        <th>QQ分组</th>
                        <th>QQ号</th>
                        <th>QQ昵称</th>
                        <th>备注</th>
                        <th>性别</th>
                    </tr>
                <?php $dnum = 0; foreach ($lists as $v): ++$dnum;?>
                    <tr>
                        <td><?=$dnum?></td>
                        <td><?=Lev::arrv('gname', $v, '未知')?>
                            <a class="editField" opname="togid" opval="<?=$v['groupid']?>" href="<?=Lev::toReRoute(['qzone/move-friend-to-group', 'toqq'=>$v['qq'], 'qq'=>$qq, 'tonick'=>$v['nick']])?>" title="可移动分组ID：<tips><?=$groupids?></tips><p class=date>填写移动目标ID</p>"><svg class="icon"><use xlink:href="#fa-logout"></use></svg></a>
                        </td>
                        <td><?=$v['qq']?></td>
                        <td>
                            <?=$v['nick']?>
                            <p class="scale9 transl" title="备注"><a class="editField" opname="tonick" opval="<?=$v['realname']?>" href="<?=Lev::toReRoute(['qzone/move-friend-to-group', 'toqq'=>$v['qq'], 'qq'=>$qq, 'togid'=>$v['groupid']])?>"><svg class="icon"><use xlink:href="#fa-compose"></use></svg></a>
                            <tonick><?=$v['realname']?></tonick>
                            </p>
                        </td>
                        <td><?=Lev::arrv($v['sex'], $sexs, '女'.$v['sex'])?></td>
                    </tr>
                <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>

<div class="LoadPageAjaxJS"><?=$js?></div>

</div>

<script>
    parent.jQuery('body').append(jQuery('.myFriendPage .LoadPageAjaxJS').html());
</script>