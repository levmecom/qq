<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-06 09:59
 *
 * 项目：upload  -  $  - sql.php
 *
 * 作者：liwei 
 */

!defined('INLEV') && exit('Access Denied LEV');

$charset = Lev::$app['db']['charset'];
$tableOptions = ' CHARACTER SET '.$charset.' ENGINE=MyISAM';
$tableNames = [
    '{{%qq}}'
];

//DROP TABLE IF EXISTS `pre_lev_modules`;
$sql = \lev\base\Migrationv::getLevBaseTableCreateSql();

$sql .= <<<EOF

DROP TABLE IF EXISTS `{{%qq}}`;
CREATE TABLE IF NOT EXISTS `{{%qq}}` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` bigint UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户UID',
  `suid` bigint UNSIGNED NOT NULL DEFAULT '0' COMMENT '三方站点用户UID',
  `qq` bigint UNSIGNED NOT NULL DEFAULT '0' COMMENT 'QQ号',
  `qqnick` varchar(164) NOT NULL DEFAULT '' COMMENT 'QQ昵称',
  `qqpwd` varchar(164) NOT NULL DEFAULT '' COMMENT 'QQ密码',
  `descs` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `loginmsg` mediumtext NOT NULL COMMENT '登陆返回信息',
  `settings` mediumtext NOT NULL COMMENT '通用设置',
  `qqstatus` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'QQ状态',
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态',
  `uptime` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addtime` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `qq_uid` (`qq`, `uid`)
) $tableOptions;

EOF;


return [
    0 => $sql,
    1 => $tableNames
];
