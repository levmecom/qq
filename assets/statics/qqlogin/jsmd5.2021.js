var jsmd5_2021 = {};

(function () {
    //'use strict';

    jsmd5_2021 = {
        getPwd: function (pwd, vcode) {
            i.getEncryption(pwd, '%L1', vcode, undefined);
        }
    };

    function h1() {
        return Math.round(4294967295 * Math.random())
    }

    function g1(t, e, i) {
        (!i || 4 < i) && (i = 4);
        for (var n = 0, o = e; o < e + i; o++) {
            n <<= 8, n |= t[o]
        }
        return (4294967295 & n) >>> 0
    }

    function _1(t, e, i) {
        t[e + 3] = i >> 0 & 255, t[e + 2] = i >> 8 & 255, t[e + 1] = i >> 16 & 255, t[e + 0] = i >> 24 & 255
    }

    function m1(t) {
        if (!t) {
            return ""
        }
        for (var e = "", i = 0; i < t.length; i++) {
            var n = Number(t[i]).toString(16);
            1 == n.length && (n = "0" + n), e += n
        }
        return e
    }

    function y1(t) {
        var o = new Array(8), l = new Array(8), s = c = 0, p = !0, r = 0;
        var e = t.length, i = 0;
        0 != (r = (e + 10) % 8) && (r = 8 - r), d = new Array(e + r + 10), o[0] = 255 & (248 & h1() | r);
        for (var n = 1; n <= r; n++) {
            o[n] = 255 & h1()
        }
        r++;
        for (n = 0; n < 8; n++) {
            l[n] = 0
        }
        for (i = 1; i <= 2;) {
            r < 8 && (o[r++] = 255 & h1(), i++), 8 == r && v1()
        }
        for (n = 0; 0 < e;) {
            r < 8 && (o[r++] = t[n++], e--), 8 == r && v1()
        }
        for (i = 1; i <= 7;) {
            r < 8 && (o[r++] = 0, i++), 8 == r && v1()
        }
        return d
    }

    function v1() {
        for (var t = 0; t < 8; t++) {
            o[t] ^= p ? l[t] : d[c + t]
        }
        for (var e = function (t) {
            var e = 16, i = g1(t, 0, 4), n = g1(t, 4, 4), o = g1(u, 0, 4), a = g1(u, 4, 4), r = g1(u, 8, 4),
                l = g1(u, 12, 4), s = 0;
            for (; 0 < e--;) {
                n = (4294967295 & (n += ((i = (4294967295 & (i += (n << 4) + o ^ n + (s = (4294967295 & (s += 2654435769)) >>> 0) ^ (n >>> 5) + a)) >>> 0) << 4) + r ^ i + s ^ (i >>> 5) + l)) >>> 0
            }
            t = new Array(8);
            return _1(t, 0, i), _1(t, 4, n), t
        }(o), t = 0; t < 8; t++) {
            d[s + t] = e[t] ^ l[t], l[t] = o[t]
        }
        c = s, s += 8, r = 0, p = !1
    }

    function w1(t) {
        for (var e = 16, i = g1(t, 0, 4), n = g1(t, 4, 4), o = g1(u, 0, 4), a = g1(u, 4, 4), r = g1(u, 8, 4), l = g1(u, 12, 4), s = 3816266640; 0 < e--;) {
            i = (4294967295 & (i -= ((n = (4294967295 & (n -= (i << 4) + r ^ i + s ^ (i >>> 5) + l)) >>> 0) << 4) + o ^ n + s ^ (n >>> 5) + a)) >>> 0, s = (4294967295 & (s -= 2654435769)) >>> 0
        }
        t = new Array(8);
        return _1(t, 0, i), _(t, 4, n), t
    }

    function b1() {
        f.length;
        for (var t = 0; t < 8; t++) {
            l[t] ^= f[s + t]
        }
        return l = w1(l), s += 8, r = 0, 1
    }

    function k1(t, e) {
        var i = [];
        if (e) {
            for (var n = 0; n < t.length; n++) {
                i[n] = 255 & t.charCodeAt(n)
            }
        } else {
            for (var o = 0, n = 0; n < t.length; n += 2) {
                i[o++] = parseInt(t.substr(n, 2), 16)
            }
        }
        return i
    }

    var q = {
        "encrypt": function (t, e) {
            return m1(y1(k1(t, e)))
        }, "enAsBase64": function (t, e) {
            for (var i = y1(k1(t, e)), n = "", o = 0; o < i.length; o++) {
                n += String.fromCharCode(i[o])
            }
            return a["default"].encode(n)
        }, "decrypt": function (t) {
            return m1(function (t) {
                var e = 0, i = new Array(8), n = t.length;
                if (f = t, n % 8 != 0 || n < 16) {
                    return null
                }
                if (l = w1(t), (e = n - (r = 7 & l[0]) - 10) < 0) {
                    return null
                }
                for (var o = 0; o < i.length; o++) {
                    i[o] = 0
                }
                d = new Array(e), c = 0, s = 8, r++;
                for (var a = 1; a <= 2;) {
                    if (r < 8 && (r++, a++), 8 == r && (i = t, !b1())) {
                        return null
                    }
                }
                for (o = 0; 0 != e;) {
                    if (r < 8 && (d[o] = 255 & (i[c + r] ^ l[r]), o++, e--, r++), 8 == r && (i = t, c = s - 8, !b1())) {
                        return null
                    }
                }
                for (a = 1; a < 8; a++) {
                    if (r < 8) {
                        if (0 != (i[c + r] ^ l[r])) {
                            return null
                        }
                        r++
                    }
                    if (8 == r && (i = t, c = s, !b1())) {
                        return null
                    }
                }
                return d
            }(k1(t, !1)))
        }, "initkey": function (t, e) {
            u = k1(t, e)
        }, "bytesToStr": function (t) {
            for (var e = "", i = 0; i < t.length; i += 2) {
                e += String.fromCharCode(parseInt(t.substr(i, 2), 16))
            }
            return e
        }, "strToBytes": function (t, e) {
            if (!t) {
                return ""
            }
            e && (t = function (t) {
                var e, i, n = [], o = t.length;
                for (e = 0; e < o; e++) {
                    0 < (i = t.charCodeAt(e)) && i <= 127 ? n.push(t.charAt(e)) : 128 <= i && i <= 2047 ? n.push(String.fromCharCode(192 | i >> 6 & 31), String.fromCharCode(128 | 63 & i)) : 2048 <= i && i <= 65535 && n.push(String.fromCharCode(224 | i >> 12 & 15), String.fromCharCode(128 | i >> 6 & 63), String.fromCharCode(128 | 63 & i))
                }
                return n.join("")
            }(t));
            for (var i = [], n = 0; n < t.length; n++) {
                i[n] = t.charCodeAt(n)
            }
            return m1(i)
        }, "bytesInStr": m1, "dataFromStr": k1
    };
    var n_entry = function () {
        function o() {
            this.n = null, this.e = 0, this.d = null, this.p = null, this.q = null, this.dmp1 = null, this.dmq1 = null, this.coeff = null
        }

        o.prototype.doPublic = function (t) {
            return t.modPowInt(this.e, this.n)
        }, o.prototype.setPublic = function (t, e) {
            null != t && null != e && 0 < t.length && 0 < e.length ? (this.n = new m(t, 16), this.e = parseInt(e, 16)) : uv_alert("Invalid RSA public key")
        }, o.prototype.encrypt = function (t) {
            return null == (t = function (t, e) {
                if (e < t.length + 11) {
                    return uv_alert("Message too long for RSA"), null
                }
                for (var i = [], n = t.length - 1; 0 <= n && 0 < e;) {
                    var o = t.charCodeAt(n--);
                    i[--e] = o
                }
                i[--e] = 0;
                for (var a = new w, r = []; 2 < e;) {
                    for (r[0] = 0; 0 == r[0];) {
                        a.nextBytes(r)
                    }
                    i[--e] = r[0]
                }
                return i[--e] = 2, i[--e] = 0, new m(i)
            }(t, this.n.bitLength() + 7 >> 3)) || null == (t = this.doPublic(t)) ? null : 0 == (1 & (t = t.toString(16)).length) ? t : "0" + t
        };

        function m(t, e, i) {
            null != t && ("number" == typeof t ? this.fromNumber(t, e, i) : null == e && "string" != typeof t ? this.fromString(t, 256) : this.fromString(t, e))
        }

        function y() {
            return new m(null)
        }

        t = (m.prototype.am = function (t, e, i, n, o, a) {
            for (var r = 16383 & e, l = e >> 14; 0 <= --a;) {
                var s = 16383 & this[t], u = this[t++] >> 14, c = l * s + u * r;
                o = ((s = r * s + ((16383 & c) << 14) + i[n] + o) >> 28) + (c >> 14) + l * u, i[n++] = 268435455 & s
            }
            return o
        }, 28), m.prototype.DB = t, m.prototype.DM = (1 << t) - 1, m.prototype.DV = 1 << t;
        m.prototype.FV = Math.pow(2, 52), m.prototype.F1 = 52 - t, m.prototype.F2 = 2 * t - 52;
        for (var e, i = "0123456789abcdefghijklmnopqrstuvwxyz", l = [], n = "0".charCodeAt(0), a = 0; a <= 9; ++a) {
            l[n++] = a
        }
        for (n = "a".charCodeAt(0), a = 10; a < 36; ++a) {
            l[n++] = a
        }
        for (n = "A".charCodeAt(0), a = 10; a < 36; ++a) {
            l[n++] = a
        }

        function s(t) {
            return i.charAt(t)
        }

        function r(t) {
            var e = y();
            return e.fromInt(t), e
        }

        function v(t) {
            var e, i = 1;
            return 0 != (e = t >>> 16) && (t = e, i += 16), 0 != (e = t >> 8) && (t = e, i += 8), 0 != (e = t >> 4) && (t = e, i += 4), 0 != (e = t >> 2) && (t = e, i += 2), 0 != (e = t >> 1) && (t = e, i += 1), i
        }

        function u(t) {
            this.m = t
        }

        function c(t) {
            this.m = t, this.mp = t.invDigit(), this.mpl = 32767 & this.mp, this.mph = this.mp >> 15, this.um = (1 << t.DB - 15) - 1, this.mt2 = 2 * t.t
        }

        function d() {
            var t;
            t = (new Date).getTime(), f[p++] ^= 255 & t, f[p++] ^= t >> 8 & 255, f[p++] ^= t >> 16 & 255, f[p++] ^= t >> 24 & 255, k <= p && (p -= k)
        }

        if (u.prototype.convert = function (t) {
            return t.s < 0 || 0 <= t.compareTo(this.m) ? t.mod(this.m) : t
        }, u.prototype.revert = function (t) {
            return t
        }, u.prototype.reduce = function (t) {
            t.divRemTo(this.m, null, t)
        }, u.prototype.mulTo = function (t, e, i) {
            t.multiplyTo(e, i), this.reduce(i)
        }, u.prototype.sqrTo = function (t, e) {
            t.squareTo(e), this.reduce(e)
        }, c.prototype.convert = function (t) {
            var e = y();
            return t.abs().dlShiftTo(this.m.t, e), e.divRemTo(this.m, null, e), t.s < 0 && 0 < e.compareTo(m.ZERO) && this.m.subTo(e, e), e
        }, c.prototype.revert = function (t) {
            var e = y();
            return t.copyTo(e), this.reduce(e), e
        }, c.prototype.reduce = function (t) {
            for (; t.t <= this.mt2;) {
                t[t.t++] = 0
            }
            for (var e = 0; e < this.m.t; ++e) {
                var i = 32767 & t[e],
                    n = i * this.mpl + ((i * this.mph + (t[e] >> 15) * this.mpl & this.um) << 15) & t.DM;
                for (t[i = e + this.m.t] += this.m.am(0, n, t, e, 0, this.m.t); t[i] >= t.DV;) {
                    t[i] -= t.DV, t[++i]++
                }
            }
            t.clamp(), t.drShiftTo(this.m.t, t), 0 <= t.compareTo(this.m) && t.subTo(this.m, t)
        }, c.prototype.mulTo = function (t, e, i) {
            t.multiplyTo(e, i), this.reduce(i)
        }, c.prototype.sqrTo = function (t, e) {
            t.squareTo(e), this.reduce(e)
        }, m.prototype.copyTo = function (t) {
            for (var e = this.t - 1; 0 <= e; --e) {
                t[e] = this[e]
            }
            t.t = this.t, t.s = this.s
        }, m.prototype.fromInt = function (t) {
            this.t = 1, this.s = t < 0 ? -1 : 0, 0 < t ? this[0] = t : t < -1 ? this[0] = t + DV : this.t = 0
        }, m.prototype.fromString = function (t, e) {
            var i;
            if (16 == e) {
                i = 4
            } else {
                if (8 == e) {
                    i = 3
                } else {
                    if (256 == e) {
                        i = 8
                    } else {
                        if (2 == e) {
                            i = 1
                        } else {
                            if (32 == e) {
                                i = 5
                            } else {
                                if (4 != e) {
                                    return void this.fromRadix(t, e)
                                }
                                i = 2
                            }
                        }
                    }
                }
            }
            this.t = 0, this.s = 0;
            for (var n = t.length, o = !1, a = 0; 0 <= --n;) {
                var r = 8 == i ? 255 & t[n] : (r = n, null == (r = l[t.charCodeAt(r)]) ? -1 : r);
                r < 0 ? "-" == t.charAt(n) && (o = !0) : (o = !1, 0 == a ? this[this.t++] = r : a + i > this.DB ? (this[this.t - 1] |= (r & (1 << this.DB - a) - 1) << a, this[this.t++] = r >> this.DB - a) : this[this.t - 1] |= r << a, (a += i) >= this.DB && (a -= this.DB))
            }
            8 == i && 0 != (128 & t[0]) && (this.s = -1, 0 < a && (this[this.t - 1] |= (1 << this.DB - a) - 1 << a)), this.clamp(), o && m.ZERO.subTo(this, this)
        }, m.prototype.clamp = function () {
            for (var t = this.s & this.DM; 0 < this.t && this[this.t - 1] == t;) {
                --this.t
            }
        }, m.prototype.dlShiftTo = function (t, e) {
            for (var i = this.t - 1; 0 <= i; --i) {
                e[i + t] = this[i]
            }
            for (i = t - 1; 0 <= i; --i) {
                e[i] = 0
            }
            e.t = this.t + t, e.s = this.s
        }, m.prototype.drShiftTo = function (t, e) {
            for (var i = t; i < this.t; ++i) {
                e[i - t] = this[i]
            }
            e.t = Math.max(this.t - t, 0), e.s = this.s
        }, m.prototype.lShiftTo = function (t, e) {
            for (var i = t % this.DB, n = this.DB - i, o = (1 << n) - 1, a = Math.floor(t / this.DB), r = this.s << i & this.DM, l = this.t - 1; 0 <= l; --l) {
                e[l + a + 1] = this[l] >> n | r, r = (this[l] & o) << i
            }
            for (l = a - 1; 0 <= l; --l) {
                e[l] = 0
            }
            e[a] = r, e.t = this.t + a + 1, e.s = this.s, e.clamp()
        }, m.prototype.rShiftTo = function (t, e) {
            e.s = this.s;
            var i = Math.floor(t / this.DB);
            if (i >= this.t) {
                e.t = 0
            } else {
                var n = t % this.DB, o = this.DB - n, a = (1 << n) - 1;
                e[0] = this[i] >> n;
                for (var r = i + 1; r < this.t; ++r) {
                    e[r - i - 1] |= (this[r] & a) << o, e[r - i] = this[r] >> n
                }
                0 < n && (e[this.t - i - 1] |= (this.s & a) << o), e.t = this.t - i, e.clamp()
            }
        }, m.prototype.subTo = function (t, e) {
            for (var i = 0, n = 0, o = Math.min(t.t, this.t); i < o;) {
                n += this[i] - t[i], e[i++] = n & this.DM, n >>= this.DB
            }
            if (t.t < this.t) {
                for (n -= t.s; i < this.t;) {
                    n += this[i], e[i++] = n & this.DM, n >>= this.DB
                }
                n += this.s
            } else {
                for (n += this.s; i < t.t;) {
                    n -= t[i], e[i++] = n & this.DM, n >>= this.DB
                }
                n -= t.s
            }
            e.s = n < 0 ? -1 : 0, n < -1 ? e[i++] = this.DV + n : 0 < n && (e[i++] = n), e.t = i, e.clamp()
        }, m.prototype.multiplyTo = function (t, e) {
            var i = this.abs(), n = t.abs(), o = i.t;
            for (e.t = o + n.t; 0 <= --o;) {
                e[o] = 0
            }
            for (o = 0; o < n.t; ++o) {
                e[o + i.t] = i.am(0, n[o], e, o, 0, i.t)
            }
            e.s = 0, e.clamp(), this.s != t.s && m.ZERO.subTo(e, e)
        }, m.prototype.squareTo = function (t) {
            for (var e = this.abs(), i = t.t = 2 * e.t; 0 <= --i;) {
                t[i] = 0
            }
            for (i = 0; i < e.t - 1; ++i) {
                var n = e.am(i, e[i], t, 2 * i, 0, 1);
                (t[i + e.t] += e.am(i + 1, 2 * e[i], t, 2 * i + 1, n, e.t - i - 1)) >= e.DV && (t[i + e.t] -= e.DV, t[i + e.t + 1] = 1)
            }
            0 < t.t && (t[t.t - 1] += e.am(i, e[i], t, 2 * i, 0, 1)), t.s = 0, t.clamp()
        }, m.prototype.divRemTo = function (t, e, i) {
            var n = t.abs();
            if (!(n.t <= 0)) {
                var o = this.abs();
                if (o.t < n.t) {
                    return null != e && e.fromInt(0), void (null != i && this.copyTo(i))
                }
                null == i && (i = y());
                var a = y(), r = this.s, l = t.s, t = this.DB - v(n[n.t - 1]);
                0 < t ? (n.lShiftTo(t, a), o.lShiftTo(t, i)) : (n.copyTo(a), o.copyTo(i));
                var s = a.t, u = a[s - 1];
                if (0 != u) {
                    var o = u * (1 << this.F1) + (1 < s ? a[s - 2] >> this.F2 : 0), c = this.FV / o,
                        d = (1 << this.F1) / o,
                        f = 1 << this.F2, p = i.t, h = p - s, g = null == e ? y() : e;
                    for (a.dlShiftTo(h, g), 0 <= i.compareTo(g) && (i[i.t++] = 1, i.subTo(g, i)), m.ONE.dlShiftTo(s, g), g.subTo(a, a); a.t < s;) {
                        a[a.t++] = 0
                    }
                    for (; 0 <= --h;) {
                        var _ = i[--p] == u ? this.DM : Math.floor(i[p] * c + (i[p - 1] + f) * d);
                        if ((i[p] += a.am(0, _, i, h, 0, s)) < _) {
                            for (a.dlShiftTo(h, g), i.subTo(g, i); i[p] < --_;) {
                                i.subTo(g, i)
                            }
                        }
                    }
                    null != e && (i.drShiftTo(s, e), r != l && m.ZERO.subTo(e, e)), i.t = s, i.clamp(), 0 < t && i.rShiftTo(t, i), r < 0 && m.ZERO.subTo(i, i)
                }
            }
        }, m.prototype.invDigit = function () {
            if (this.t < 1) {
                return 0
            }
            var t = this[0];
            if (0 == (1 & t)) {
                return 0
            }
            var e = 3 & t;
            return 0 < (e = (e = (e = (e = e * (2 - (15 & t) * e) & 15) * (2 - (255 & t) * e) & 255) * (2 - ((65535 & t) * e & 65535)) & 65535) * (2 - t * e % this.DV) % this.DV) ? this.DV - e : -e
        }, m.prototype.isEven = function () {
            return 0 == (0 < this.t ? 1 & this[0] : this.s)
        }, m.prototype.exp = function (t, e) {
            if (4294967295 < t || t < 1) {
                return m.ONE
            }
            var i, n = y(), o = y(), a = e.convert(this), r = v(t) - 1;
            for (a.copyTo(n); 0 <= --r;) {
                e.sqrTo(n, o), 0 < (t & 1 << r) ? e.mulTo(o, a, n) : (i = n, n = o, o = i)
            }
            return e.revert(n)
        }, m.prototype.toString = function (t) {
            if (this.s < 0) {
                return "-" + this.negate().toString(t)
            }
            var e;
            if (16 == t) {
                e = 4
            } else {
                if (8 == t) {
                    e = 3
                } else {
                    if (2 == t) {
                        e = 1
                    } else {
                        if (32 == t) {
                            e = 5
                        } else {
                            if (4 != t) {
                                return this.toRadix(t)
                            }
                            e = 2
                        }
                    }
                }
            }
            var i, n = (1 << e) - 1, o = !1, a = "", r = this.t, l = this.DB - r * this.DB % e;
            if (0 < r--) {
                for (l < this.DB && 0 < (i = this[r] >> l) && (o = !0, a = s(i)); 0 <= r;) {
                    l < e ? (i = (this[r] & (1 << l) - 1) << e - l, i |= this[--r] >> (l += this.DB - e)) : (i = this[r] >> (l -= e) & n, l <= 0 && (l += this.DB, --r)), 0 < i && (o = !0), o && (a += s(i))
                }
            }
            return o ? a : "0"
        }, m.prototype.negate = function () {
            var t = y();
            return m.ZERO.subTo(this, t), t
        }, m.prototype.abs = function () {
            return this.s < 0 ? this.negate() : this
        }, m.prototype.compareTo = function (t) {
            var e = this.s - t.s;
            if (0 != e) {
                return e
            }
            var i = this.t;
            if (0 != (e = i - t.t)) {
                return e
            }
            for (; 0 <= --i;) {
                if (0 != (e = this[i] - t[i])) {
                    return e
                }
            }
            return 0
        }, m.prototype.bitLength = function () {
            return this.t <= 0 ? 0 : this.DB * (this.t - 1) + v(this[this.t - 1] ^ this.s & this.DM)
        }, m.prototype.mod = function (t) {
            var e = y();
            return this.abs().divRemTo(t, null, e), this.s < 0 && 0 < e.compareTo(m.ZERO) && t.subTo(e, e), e
        }, m.prototype.modPowInt = function (t, e) {
            return e = new (t < 256 || e.isEven() ? u : c)(e), this.exp(t, e)
        }, m.ZERO = r(0), m.ONE = r(1), null == f) {
            var f = [], p = 0;
            for (; p < k;) {
                g = Math.floor(65536 * Math.random()), f[p++] = g >>> 8, f[p++] = 255 & g
            }
            p = 0, d()
        }

        function _() {
            if (null == e) {
                for (d(), (e = new b).init(f), p = 0; p < f.length; ++p) {
                    f[p] = 0
                }
                p = 0
            }
            return e.next()
        }

        function w() {
        }

        function b() {
            this.i = 0, this.j = 0, this.S = []
        }

        w.prototype.nextBytes = function (t) {
            for (var e = 0; e < t.length; ++e) {
                t[e] = _()
            }
        }, b.prototype.init = function (t) {
            for (var e, i, n = 0; n < 256; ++n) {
                this.S[n] = n
            }
            for (n = e = 0; n < 256; ++n) {
                e = e + this.S[n] + t[n % t.length] & 255, i = this.S[n], this.S[n] = this.S[e], this.S[e] = i
            }
            this.i = 0, this.j = 0
        }, b.prototype.next = function () {
            var t;
            return this.i = this.i + 1 & 255, this.j = this.j + this.S[this.i] & 255, t = this.S[this.i], this.S[this.i] = this.S[this.j], this.S[this.j] = t, this.S[t + this.S[this.i] & 255]
        };
        var k = 256;
        return {
            "rsa_encrypt": function (t, e, i) {
                var n = new o;
                return n.setPublic("e9a815ab9d6e86abbf33a4ac64e9196d5be44a09bd0ed6ae052914e1a865ac8331fed863de8ea697e9a7f63329e5e23cda09c72570f46775b7e39ea9670086f847d3c9c51963b131409b1e04265d9747419c635404ca651bbcbc87f99b8008f7f5824653e3658be4ba73e4480156b390bb73bc1f8b33578e7a4e12440e9396f2552c1aff1c92e797ebacdc37c109ab7bce2367a19c56a033ee04534723cc2558cb27368f5b9d32c04d12dbd86bbd68b1d99b7c349a8453ea75d1b2e94491ab30acf6c46a36a75b721b312bedf4e7aad21e54e9bcbcf8144c79b6e3c05eb4a1547750d224c0085d80e6da3907c3d945051c13c7c1dcefd6520ee8379c4f5231ed", "10001"), n.encrypt(t)
            }
        }
    }();

    function a_encode(t, e) {
        e = t.charCodeAt(e);
        if (255 < e) {
            throw"INVALID_CHARACTER_ERR: DOM Exception 5"
        }
        return e
    }

    var s_encode = {
        "PADCHAR": "=",
        "ALPHA": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        "getbyte": function (t, e) {
            e = t.charCodeAt(e);
            if (255 < e) {
                throw"INVALID_CHARACTER_ERR: DOM Exception 5"
            }
            return e
        },
        "encode": function (t) {
            if (1 != arguments.length) {
                throw"SyntaxError: Not enough arguments"
            }
            var e, i, n = s_encode.PADCHAR, o = s_encode.ALPHA, a = s_encode.getbyte, r = [],
                l = (t = "" + t).length - t.length % 3;
            if (0 == t.length) {
                return t
            }
            for (e = 0; e < l; e += 3) {
                i = a_encode(t, e) << 16 | a_encode(t, e + 1) << 8 | a_encode(t, e + 2), r.push(o.charAt(i >> 18)), r.push(o.charAt(i >> 12 & 63)), r.push(o.charAt(i >> 6 & 63)), r.push(o.charAt(63 & i))
            }
            switch (t.length - l) {
                case 1:
                    i = a_encode(t, e) << 16, r.push(o.charAt(i >> 18) + o.charAt(i >> 12 & 63) + n + n);
                    break;
                case 2:
                    i = a_encode(t, e) << 16 | a_encode(t, e + 1) << 8, r.push(o.charAt(i >> 18) + o.charAt(i >> 12 & 63) + o.charAt(i >> 6 & 63) + n)
            }
            return r.join("")
        }
    };
    var o, a, d, i = (o = 1, a = 8, d = 32, {
        "getEncryption": function (t, e, i, n) {
            i = i || "", t = t || "";
            for (var n = n ? t : u(t), t = u(w(n) + e), i = q.strToBytes(i.toUpperCase(), !0), o = Number(i.length / 2).toString(16); o.length < 4;) {
                o = "0" + o
            }
            q.initkey(t), i = q.encrypt(n + q.strToBytes(e) + o + i), q.initkey("");
            for (var a = Number(i.length / 2).toString(16); a.length < 4;) {
                a = "0" + a
            }
            return i = n_entry.rsa_encrypt(w(a + i)), setTimeout(function () {
                !function (t, e) {
                    if (!(Math.random() > (e || 1))) {
                        try {
                            var i = location.protocol + "//ui.ptlogin2.qq.com/cgi-bin/report?id=" + t;
                            document.createElement("img").src = i
                        } catch (n) {
                        }
                    }
                }(488358, 1)
            }, 0), s_encode.encode(w(i)).replace(/[\/\+=]/g, function (t) {
                return {"/": "-", "+": "*", "=": "_"}[t]
            })
        }, "getRSAEncryption": function (t, e, i) {
            return e = (i ? t : u(t)) + e.toUpperCase(), n_entry.rsa_encrypt(e)
        }, "md5": u
    });

    function u(t) {
        return v(c(y(t = t), t.length * a))
    }

    function c(t, e) {
        t[e >> 5] |= 128 << e % 32, t[14 + (e + 64 >>> 9 << 4)] = e;
        for (var i = 1732584193, n = -271733879, o = -1732584194, a = 271733878, r = 0; r < t.length; r += 16) {
            var l = i, s = n, u = o, c = a, i = p(i, n, o, a, t[r + 0], 7, -680876936),
                a = p(a, i, n, o, t[r + 1], 12, -389564586), o = p(o, a, i, n, t[r + 2], 17, 606105819),
                n = p(n, o, a, i, t[r + 3], 22, -1044525330);
            i = p(i, n, o, a, t[r + 4], 7, -176418897), a = p(a, i, n, o, t[r + 5], 12, 1200080426), o = p(o, a, i, n, t[r + 6], 17, -1473231341), n = p(n, o, a, i, t[r + 7], 22, -45705983), i = p(i, n, o, a, t[r + 8], 7, 1770035416), a = p(a, i, n, o, t[r + 9], 12, -1958414417), o = p(o, a, i, n, t[r + 10], 17, -42063), n = p(n, o, a, i, t[r + 11], 22, -1990404162), i = p(i, n, o, a, t[r + 12], 7, 1804603682), a = p(a, i, n, o, t[r + 13], 12, -40341101), o = p(o, a, i, n, t[r + 14], 17, -1502002290), i = h(i, n = p(n, o, a, i, t[r + 15], 22, 1236535329), o, a, t[r + 1], 5, -165796510), a = h(a, i, n, o, t[r + 6], 9, -1069501632), o = h(o, a, i, n, t[r + 11], 14, 643717713), n = h(n, o, a, i, t[r + 0], 20, -373897302), i = h(i, n, o, a, t[r + 5], 5, -701558691), a = h(a, i, n, o, t[r + 10], 9, 38016083), o = h(o, a, i, n, t[r + 15], 14, -660478335), n = h(n, o, a, i, t[r + 4], 20, -405537848), i = h(i, n, o, a, t[r + 9], 5, 568446438), a = h(a, i, n, o, t[r + 14], 9, -1019803690), o = h(o, a, i, n, t[r + 3], 14, -187363961), n = h(n, o, a, i, t[r + 8], 20, 1163531501), i = h(i, n, o, a, t[r + 13], 5, -1444681467), a = h(a, i, n, o, t[r + 2], 9, -51403784), o = h(o, a, i, n, t[r + 7], 14, 1735328473), i = g(i, n = h(n, o, a, i, t[r + 12], 20, -1926607734), o, a, t[r + 5], 4, -378558), a = g(a, i, n, o, t[r + 8], 11, -2022574463), o = g(o, a, i, n, t[r + 11], 16, 1839030562), n = g(n, o, a, i, t[r + 14], 23, -35309556), i = g(i, n, o, a, t[r + 1], 4, -1530992060), a = g(a, i, n, o, t[r + 4], 11, 1272893353), o = g(o, a, i, n, t[r + 7], 16, -155497632), n = g(n, o, a, i, t[r + 10], 23, -1094730640), i = g(i, n, o, a, t[r + 13], 4, 681279174), a = g(a, i, n, o, t[r + 0], 11, -358537222), o = g(o, a, i, n, t[r + 3], 16, -722521979), n = g(n, o, a, i, t[r + 6], 23, 76029189), i = g(i, n, o, a, t[r + 9], 4, -640364487), a = g(a, i, n, o, t[r + 12], 11, -421815835), o = g(o, a, i, n, t[r + 15], 16, 530742520), i = _(i, n = g(n, o, a, i, t[r + 2], 23, -995338651), o, a, t[r + 0], 6, -198630844), a = _(a, i, n, o, t[r + 7], 10, 1126891415), o = _(o, a, i, n, t[r + 14], 15, -1416354905), n = _(n, o, a, i, t[r + 5], 21, -57434055), i = _(i, n, o, a, t[r + 12], 6, 1700485571), a = _(a, i, n, o, t[r + 3], 10, -1894986606), o = _(o, a, i, n, t[r + 10], 15, -1051523), n = _(n, o, a, i, t[r + 1], 21, -2054922799), i = _(i, n, o, a, t[r + 8], 6, 1873313359), a = _(a, i, n, o, t[r + 15], 10, -30611744), o = _(o, a, i, n, t[r + 6], 15, -1560198380), n = _(n, o, a, i, t[r + 13], 21, 1309151649), i = _(i, n, o, a, t[r + 4], 6, -145523070), a = _(a, i, n, o, t[r + 11], 10, -1120210379), o = _(o, a, i, n, t[r + 2], 15, 718787259), n = _(n, o, a, i, t[r + 9], 21, -343485551), i = m(i, l), n = m(n, s), o = m(o, u), a = m(a, c)
        }
        return 16 == d ? Array(n, o) : Array(i, n, o, a)
    }

    function f(t, e, i, n, o, a) {
        return m((a = m(m(e, t), m(n, a))) << (o = o) | a >>> 32 - o, i)
    }

    function p(t, e, i, n, o, a, r) {
        return f(e & i | ~e & n, t, e, o, a, r)
    }

    function h(t, e, i, n, o, a, r) {
        return f(e & n | i & ~n, t, e, o, a, r)
    }

    function g(t, e, i, n, o, a, r) {
        return f(e ^ i ^ n, t, e, o, a, r)
    }

    function _(t, e, i, n, o, a, r) {
        return f(i ^ (e | ~n), t, e, o, a, r)
    }

    function m(t, e) {
        var i = (65535 & t) + (65535 & e);
        return (t >> 16) + (e >> 16) + (i >> 16) << 16 | 65535 & i
    }

    function y(t) {
        for (var e = Array(), i = (1 << a) - 1, n = 0; n < t.length * a; n += a) {
            e[n >> 5] |= (t.charCodeAt(n / a) & i) << n % 32
        }
        return e
    }

    function v(t) {
        for (var e = o ? "0123456789ABCDEF" : "0123456789abcdef", i = "", n = 0; n < 4 * t.length; n++) {
            i += e.charAt(t[n >> 2] >> n % 4 * 8 + 4 & 15) + e.charAt(t[n >> 2] >> n % 4 * 8 & 15)
        }
        return i
    }

    function w(t) {
        for (var e = [], i = 0; i < t.length; i += 2) {
            e.push(String.fromCharCode(parseInt(t.substr(i, 2), 16)))
        }
        return e.join("")
    }

})();
