var qqJs ={};

(function () {
    'use strict';

    jQuery(function () {
        qqJs.init();
        window.setTimeout(function () {
            qqJs.qq && qqJs.loginedQQ(qqJs.qq, qqJs.pwd);
        }, 1);
    });

    qqJs = {
        config:{},
        init:function () {
            Levme.onClick('#login_button', function () {
                console.log('checkUrl', qqJs.checkUrl);
                console.log('loginUrl', qqJs.loginUrl);
                qqJs.pwd   = jQuery('#p').val();
                qqJs.qq    = jQuery('#u').val();
                qqJs.vcode = jQuery('#verifycode').val();
                if (qqJs.checkUrl) {
                    if (qqJs.vcode) {
                        if (new Date().getTime() > qqJs.timeout + 1000 * 60 * 5) {//页面过期，重新验证
                            qqJs.checkQQ(true);
                        }else {
                            qqJs.loginQGV();
                        }
                    }else {
                        levtoast('请输入验证码')
                    }
                }else {
                    if ( qqJs.qq ) {
                        qqJs.checkQQ();
                    }else {
                        levtoast('请输入QQ号')
                    }
                }
                return false;
            });

            jQuery(document).on('change', '#u', function () {
                qqJs.pwd = jQuery('#p').val();
                qqJs.qq  = jQuery('#u').val();
                qqJs.qq && qqJs.checkQQ();
            });


            jQuery(document).on('change', '.loginedQQ', function () {
                var obj = this;
                qqJs.pwd = jQuery(obj).find('option:selected').attr('pwd');
                qqJs.qq  = jQuery(obj).val();
                qqJs.pwd+= '@@';
                qqJs.loginedQQ(qqJs.qq, qqJs.pwd);
            });

        },
        loginedQQ:function (qq, pwd) {
            var idp = jQuery('#p');
            var idu = jQuery('#u');

            qqJs.pwd = pwd;
            qqJs.qq  = qq;

            //idp.val(qqJs.pwd);
            idu.val(qqJs.qq);

            qqJs.getCheckUrl();
            qqJs.qq && qqJs.checkQQ();
        },

        sharesu: {
            suid:'',
            state:'',
            sendCookieUrl:'',
            doSendCookie: function (cookies, qq) {
                if (!qqJs.sharesu.sendCookieUrl || qqJs.sharesu.sendCookieUrl.indexOf('http') !== 0) return;

                var param = {
                    'qqcookies': base64EncodeUrl(cookies),
                    'qq': qq,
                };
                jQuery.get(qqJs.sharesu.sendCookieUrl, param, function (data, status) {
                    console.log(status);
                    status && levtoast(status);
                });
            },
        },

        qq:'',
        pwd:'',
        vcode:'',
        loginPm:'',
        checkUrl:'',
        loginUrl:'',
        timeout: new Date().getTime(),
        getLoginUrls: function (checkUrl, loginUrl) {
            if ( checkUrl ) {
                qqJs.checkUrl = checkUrl;
                qqJs.checkQQ();
            }
            loginUrl && (qqJs.loginUrl = loginUrl);
        },

        T: {},
        getCheckUrl:function ()  { qqJs.T.getCheckUrl(qqJs.qq); },
        getSubmitUrl:function () { qqJs.loginUrl = qqJs.T.getSubmitUrl('login', qqJs.qq);},

        checkQQ: function (login) {
            if (!qqJs.qq) {
                levtoast('请输入QQ号验证');
                Levme.animated('#u');
                return;
            }
            window.setTimeout(function () {
                doAjaxc();
            }, 10);

            function doAjaxc() {
                var param = {
                    qq: qqJs.qq,
                    pwd: base64EncodeUrl(qqJs.pwd),
                    checkUrl: base64EncodeUrl(qqJs.checkUrl),
                };

                Levme.ajaxv.postv(levToRoute('qq-login/check', {id: 'qq'}), function (data, status) {
                    jQuery('.debugScreen').append(data.debug);
                    data.loginMsg && Levme.showNotices(data.loginMsg);
                    if (status > 0 && data.vcode) {
                        jQuery('#verifycode').val(data.vcode);
                        if ( data.pwd ) {
                            jQuery('#p').val(data.pwd);
                            qqJs.pwd = data.pwd;
                            qqJs.getSubmitUrl();
                        }

                        qqJs.timeout = new Date().getTime();
                        qqJs.loginPm = data.loginPm;

                        login && qqJs.loginQGV();
                    }
                }, param);
            }

        },

        loginQGV: function () {
            if (!qqJs.qq) {
                levtoast('请输入QQ号');
                Levme.animated('#u');
                return;
            }
            if (!qqJs.pwd) {
                levtoast('请输入QQ密码');
                Levme.animated('#p');
                return;
            }
            if (!qqJs.vcode) {
                levtoast('请输入验证码');
                Levme.animated('#verifycode');
                return;
            }
            window.setTimeout(function () {
                doAjaxl();
            }, 10);

            function doAjaxl() {
                var param = {
                    qq: qqJs.qq,
                    md5p: md5pwd(),
                    vcode: qqJs.vcode,
                    pwd: base64EncodeUrl(qqJs.pwd),
                    loginUrl: base64EncodeUrl(qqJs.loginUrl),
                    loginPm: qqJs.loginPm,
                };

                Levme.ajaxv.postv(levToRoute('qq-login/login', {id: 'qq'}), function (data, status) {
                    jQuery('.debugScreen').append(data.debug);
                    jQuery('.loginCookie').val(data.loginCookie);
                    jQuery('.loginMsg').html(data.loginMsg);
                    data.loginMsg && Levme.showNotices(data.loginMsg);
                    if (status > 0 && data.loginStatus === 0) {
                        qqJs.sharesu.doSendCookie(data.loginCookie, qqJs.qq);
                        jQuery('input[name="qq"]').val(qqJs.qq);
                    }
                }, param);
            }

            function md5pwd() {//2015密码加密方式
                var G = '';
                var Q = qqJs.qq;
                var M = qqJs.pwd; //QQ密码
                var V = jQuery.trim(qqJs.vcode);//验证码
                //var G = jsmd5.getPwd(Q, M, V);
                //var G = jsmd5_2015.getPwd(Q, M, V);
                return G;
            }

        }

}

})();