
##

安装
-------------------

1. 在`composer.json`文件中加入如下代码
~~~
{
  "require": {
    "php": ">=5.4.0",
    "levmecom/levs": "*",
    "levmecom/qq": "*"
  },
  "scripts": {
    "post-package-install": [
      "modules\\levs\\modules\\composer\\CmdComposerHelper::postPackageInstall"
    ]
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitee.com/levmecom/levs"
    },
    {
      "type": "vcs",
      "url": "https://gitee.com/levmecom/qq"
    }
  ]
}
~~~
2.在`composer.josn`文件目录，执行cmd命令： 

~~~
composer create-project
~~~
