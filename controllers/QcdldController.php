<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-09 22:09
 *
 * 项目：levs  -  $  - QcdldController.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\controllers;

use Lev;
use lev\base\Controllerv;
use modules\qq\widgets\qcdld\qcdldGameHelper;

class QcdldController extends Controllerv
{

    public static function actionIndex() {
        $qq = Lev::stripTags(Lev::GPv('qq')) ?: '227248948';
        qcdldGameHelper::myFriend($qq);
    }

}