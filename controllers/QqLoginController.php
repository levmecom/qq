<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-09 00:05
 *
 * 项目：levs  -  $  - QqLoginController.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\controllers;

use Lev;
use lev\base\Assetsv;
use lev\base\Controllerv;
use lev\base\Viewv;
use lev\helpers\curlHelper;
use lev\helpers\UrlHelper;
use modules\qq\helpers\qqLoginHelper;
use modules\qq\helpers\qquserCacheHelper;
use modules\qq\helpers\UrlQqHelper;
use modules\qq\widgets\qqlogin\qqLoginWidget;

class QqLoginController extends Controllerv
{

    public static function actionIndex($logintype = null) {
        !$logintype &&
        $logintype = Lev::stripTags(Lev::GPv('opid'));

        $qq  = floatval(Lev::GPv('qq')) ?: '';

        $qqs = [];
        if (Lev::$app['uid'] >0) {
            $qqs = qqLoginWidget::myqqs(Lev::$app['uid']);
        }

        Lev::$app['title'] = 'QQ协议登陆';

        Assetsv::animateCss();
        Assetsv::ajaxFormJs();
        Viewv::render('qq-login/index', [
            'qqs' => $qqs,
            'qq'  => $qq,
            'show'  => true,
            'logintype'  => $logintype,
            'mySendCookieUrl' => qquserCacheHelper::mySendCookieUrl(Lev::$app['uid']),
        ]);
    }

    /**
     * QQ空间协议登陆
     */
    public static function actionQzone() {
        static::actionIndex('qzone');
    }

    /**
     * Q宠大乐斗游戏协议登陆
     */
    public static function actionQcdld() {
        static::actionIndex('qcdld');
    }

    public static function actionShare() {
        Lev::$app['title'] = 'QQ协议登陆';

        $uid = floatval(Lev::GPv('opid'));
        $suid = floatval(Lev::GPv('suid'));
        $state = Lev::stripTags(Lev::GPv('state'));

        $mySendCookieUrl = UrlQqHelper::getSendCookieUrl($uid, $suid, $state);

        $qqs = [];
        if (Lev::$app['uid'] >0) {
            $qqs = qqLoginWidget::myqqs(Lev::$app['uid']);
        }

        Viewv::render('qq-login/share', [
            'mySendCookieUrl' => $mySendCookieUrl,
            'suid'  => $suid,
            'state' => $state,
            'qqs'   => $qqs,
        ]);
    }


    public static function actionSendQqurl() {
        $gbk = Lev::GPv('gbk');
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $postpm = Lev::stripTags(Lev::GPv('postpm'));
        $qqurl = Lev::stripTags(Lev::GPv('qqurl'));
        if (!UrlHelper::check($qqurl)) {
            Lev::showMessages(Lev::responseMsg(-4, '抱歉，验证网址必须以http://或https://开头'));
        }
        $param['url']  = $qqurl;
        $param['time'] = 5;
        $param['post'] = $postpm;
        $param['ip']   = qqLoginHelper::loginIp();
        $param['cookiefile'] = qqLoginHelper::createCK($qq);
        $res = curlHelper::doCurl($param);
        //$msg = Lev::responseMsg();
        //$msg['message'] = $res;
        $gbk && $res = iconv('GBK', 'UTF-8//IGNORE', $res);
        Assetsv::Jquery();
        Lev::showMessage($res);
    }

    public static function actionSaveSendCookieUrl() {
        if (Lev::$app['uid'] <1) {
            Lev::showMessages(Lev::responseMsg(-5, '请先登陆'));
        }
        $link = Lev::stripTags(Lev::GPv('link'));
        if ($link && !UrlHelper::check($link)) {
            Lev::showMessages(Lev::responseMsg(-4, '抱歉，必须以http://或https://开头'));
        }
        qquserCacheHelper::mySendCookieUrl(Lev::$app['uid'], $link);
        Lev::showMessages(Lev::responseMsg(1, '保存成功', ['notReload'=>1]));
    }


    public static function actionCheck() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $pwd = Lev::stripTags(Lev::base64_decode_url(Lev::GPv('pwd')));
        $checkUrl = Lev::stripTags(Lev::base64_decode_url(Lev::GPv('checkUrl')));
        $checkRes = qqLoginHelper::check($qq, $pwd, $checkUrl);
        Lev::showMessages(Lev::responseMsg(1, '', $checkRes));
    }


    public static function actionLogin() {
        //$checkUrl = Lev::stripTags(Lev::base64_decode_url(Lev::GPv('checkUrl')));
        $loginUrl = Lev::stripTags(Lev::base64_decode_url(Lev::GPv('loginUrl')));
        $loginPm = json_decode(Lev::stripTags(Lev::base64_decode_url(Lev::GPv('loginPm'))), true);
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $pwd = Lev::stripTags(Lev::base64_decode_url(Lev::GPv('pwd')));
        $loginRes = qqLoginHelper::cloginUrl($qq, $pwd, $loginUrl, $loginPm);
        Lev::showMessages(Lev::responseMsg(1, '', $loginRes));
    }

}