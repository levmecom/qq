<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 11:18
 *
 * 项目：levs  -  $  - QqController.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\controllers;

use Lev;
use lev\base\Controllerv;
use lev\base\Viewv;
use modules\qq\helpers\qqLoginHelper;
use modules\qq\table\qq\qqModelHelper;
use modules\qq\widgets\qcdld\qcdldGameHelper;
use modules\qq\widgets\qzone\qzoneHelper;

class QqController extends Controllerv
{

    public static function actionMyfriend() {
        $force = Lev::stripTags(Lev::GPv('force'));
        $qq = Lev::stripTags(Lev::GPv('qq'));
        if (!$qq) {
            Lev::showMessages(Lev::responseMsg(-300100, 'QQ号不能为空'));
        }

        $result = qqLoginHelper::getQqdatas($qq);
        !$force && !$result && $force = 1;
        $js = '';
        if ($force) {
            //$result = qcdldGameHelper::myFriend($qq);
            $result = qzoneHelper::myGroupFriend($qq);
            if (!empty($result['hynum'])) {
                !empty($result['groups']) && qqLoginHelper::setQqdata($qq, 'groups', $result['groups']);
                qqLoginHelper::updateHynum($qq, $result['hynum']);
                qqLoginHelper::updateMyFriend($qq, $result['lists']);
            }
            $online = empty($result['qqstatus']);
            qqModelHelper::update(['qqstatus'=>$online ? 5 : 4], ['qq'=>$qq, 'uid'=>Lev::$app['uid']]);

            $onlineMsg = qqModelHelper::qqstatusCheckHtm(0,0, $online);
            $js = '<script>jQuery(".qq-'.$qq.' qqstatus").html("'.$onlineMsg.'")</script>';
            $online &&
            $js.= '<script>jQuery(".qq-'.$qq.' hynum").html('.$result['hynum'].')</script>';
        }else {
            $result['lists'] = qqLoginHelper::getMyFriend($qq);
        }
        if (Lev::isAjax()) {
            Lev::showMessages(Lev::responseMsg(1, '刷新完成！'.$qq.$onlineMsg.$js, $result));
        }
        Viewv::render('qq/myfriend', [
            'js'    => $js,
            'qq'    => $qq,
            'lists' => $result['lists'],
            'groups'=> $result['groups'],
            'sexs'  => qqModelHelper::sex(),
        ]);
    }

}