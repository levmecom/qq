<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 01:11
 *
 * 项目：levs  -  $  - QzoneController.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\controllers;

use Lev;
use lev\base\Controllerv;
use modules\qq\helpers\qqLoginHelper;
use modules\qq\table\qq\qqModelHelper;
use modules\qq\widgets\qzone\qzoneHelper;

class QzoneController extends Controllerv
{
    public static function actionSendGift() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $sendqq = Lev::stripTags(Lev::GPv('sendqq'));
        $sendmsg = Lev::stripTags(Lev::GPv('sendmsg'));

        !$sendqq && $sendqq = explode('||', $sendmsg)[0];
        $sendqq = trim($sendqq);
        $res = qzoneHelper::sendGift($qq, $sendqq, $sendmsg);
        Lev::showMessages($res);
    }

    public static function actionEditSignAndDongtai() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $signmsg = Lev::stripTags(Lev::GPv('signmsg'));
        $res = qzoneHelper::editSignAndDongtai($qq, $signmsg);
        Lev::showMessages($res);
    }

    public static function actionAddFriend() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $addqq = Lev::stripTags(Lev::GPv('addqq'));
        $res = qzoneHelper::addMayFriend($qq, $addqq);
        Lev::showMessages($res);
    }

    public static function actionMyqqGroups() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $force = Lev::stripTags(Lev::GPv('force'));

        $qqgroups = qqLoginHelper::getQqdata($qq, 'qqgroups');
        if ($force || empty($qqgroups)) {
            $res = qzoneHelper::myqqGroups($qq);
            $qqgroups = $res['qqgroups'];
            $res['status'] >0 && qqLoginHelper::setQqdata($qq, 'qqgroups', $res['qqgroups']);
        }

        $htm = '<pre style="font-size:12px">'.print_r($qqgroups, true).'</pre>';
        Lev::showMessages($htm, $res['status'], ['刷新'=>Lev::toCurrent(['force'=>1], false, false)]);
    }

    public static function actionMoveFriendToGroup() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $toqq = Lev::stripTags(Lev::GPv('toqq'));
        $togid = Lev::stripTags(Lev::GPv('togid'));
        $tonick = Lev::stripTags(Lev::GPv('tonick'));

        $res = qzoneHelper::moveFriendToGroup($qq, $toqq, $togid, $tonick);
        Lev::showMessages($res);
    }

    public static function actionMyGroupFriend() {
        $qq = Lev::stripTags(Lev::GPv('qq'));

        $res = qzoneHelper::myGroupFriend($qq);
        Lev::showMessages($res);
    }

    public static function actionMyFriend() {//Lev::debug();
        $qq = Lev::stripTags(Lev::GPv('qq'));//好友最多显示200

        $res = qzoneHelper::myFriend($qq);
        Lev::showMessages($res);
    }

    public static function actionEditNick() {
        $qq = Lev::stripTags(Lev::GPv('qq'));
        $nick = Lev::stripTags(Lev::GPv('nick'));
        if (!Lev::GPv('doit')) {
            Lev::showMessages(Lev::responseMsg(3, '您确定要修改吗？', ['tourl'=>Lev::toCurrent(['doit'=>1], true, false)]));
        }
        $res = qzoneHelper::editMyNick($qq, $nick);
        if ($res['status'] >0) {
            qqModelHelper::update(['qqnick'=>$nick], ['qq'=>$qq]);
        }
        Lev::showMessages($res);
    }
}