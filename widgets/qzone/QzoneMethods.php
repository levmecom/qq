<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 00:53
 *
 * 项目：levs  -  $  - QzoneMethods.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\widgets\qzone;

use Lev;
use modules\qq\widgets\qcdld\qcdldGameHelper;

class QzoneMethods extends BaseQzones
{

    /**
     * 赠送礼物
     * qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fgift%2Fsend_list.html%3Fuin%3D359025543%26source%3D%26nick%3D%26popupsrc%3D600&fupdate=1&random=0.6633583444106722&charset=utf-8&uin=40529743&targetuin=819005686_1249138194_1540564901&source=0&giftid=131003&private=0&giveback=&qzoneflag=1&time=&timeflag=0&giftmessage=667788999&gifttype=0&gifttitle=%E4%B8%BA%E7%88%B1%E6%AF%94%E5%BF%83+&traceid=&newadmin=1&birthdaytab=0&answerid=&arch=0&clicksrc=&click_src_v2=01%7C01%7C600%7C0556%7C0000%7C01
     * @param $qq
     * @param string $sendqq  eg: 819005686_1249138194_1540564901
     * @return array
     */
    public static function sendGift($qq, $sendqq, $sendmsg = '生日快乐！为你比心~') {
        $pm['url'] = 'https://user.qzone.qq.com/proxy/domain/drift.qzone.qq.com/cgi-bin/sendgift?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fgift%2Fsend_list.html%3Fuin%3D359025543%26source%3D%26nick%3D%26popupsrc%3D600&fupdate=1&random=0.6633583444106722&charset=utf-8&uin='.$qq.'&targetuin='.$sendqq.'&source=0&giftid=131003&private=0&giveback=&qzoneflag=1&time=&timeflag=0&giftmessage='.urlencode($sendmsg).'&gifttype=0&gifttitle=%E4%B8%BA%E7%88%B1%E6%AF%94%E5%BF%83+&traceid=&newadmin=1&birthdaytab=0&answerid=&arch=0&clicksrc=&click_src_v2=01%7C01%7C600%7C0556%7C0000%7C01';
        parse_str($poststr, $posts);
        $pm['post'] = $posts;
        $pm['referer'] = 'https://user.qzone.qq.com/'.$qq.'/311';
        $ret = static::getInfos($qq, $pm);//echo $ret;
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 同步签名请求
     * @param $qq
     * @return array
     */
    public static function setSyncSign($qq) {
        $pm['url'] = 'https://user.qzone.qq.com/proxy/domain/taotao.qzone.qq.com/cgi-bin/emotion_cgi_signset_v6?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2F'.$qq.'%2F311&code_version=1&format=fs&uin='.$qq.'&hostuin='.$qq.'&ss_sync_sign=1&out_charset=UTF-8';
        parse_str($poststr, $posts);
        $pm['post'] = $posts;
        $pm['referer'] = 'https://user.qzone.qq.com/'.$qq.'/311';
        $ret = static::getInfos($qq, $pm);//echo $ret;
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 修改签名，并发布空间动态 通知QQ好友
     * qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2F40529743%2Finfocenter%3Fvia%3Dtoolbar&syn_tweet_verson=1&paramstr=1&pic_template=&richtype=&richval=&special_url=&subrichtype=&who=1&con=qm@{uin:1274565886,nick:13213123}+@{uin:2434728084,nick:13}+@{uin:2463676975,nick:15}+到了&feedversion=1&ver=1&ugc_right=1&to_sign=1&hostuin=40529743&code_version=1&format=fs
     * @param $qq
     * @param $msg
     * @param int $tosign
     * @return array
     */
    public static function editSignAndDongtai($qq, $msg, $tosign = 1) {
        //static::setSyncSign($qq);
        $pm['url'] = 'https://user.qzone.qq.com/proxy/domain/taotao.qzone.qq.com/cgi-bin/emotion_cgi_publish_v6?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2F40529743%2Finfocenter%3Fvia%3Dtoolbar&syn_tweet_verson=1&paramstr=1&pic_template=&richtype=&richval=&special_url=&subrichtype=&who=1&con='.urlencode($msg).'&feedversion=1&ver=1&ugc_right=1&to_sign='.$tosign.'&hostuin='.$qq.'&code_version=1&format=fs';
        parse_str($poststr, $posts);
        $pm['post'] = $posts;
        $pm['referer'] = 'https://user.qzone.qq.com/'.$qq.'/311';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 拒绝好友请求
     * qzreferrer=https%3A%2F%2Frc.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fv8%2Fpages%2Ffriends%2Ffriend_msg_setting.html%3Fmode%3Drefuse%26ouin%3D3008868552%26id%3D1%26flag%3D100%26key%3D3008868552%26time%3D1624433913%26g_iframeUser%3D1%26g_iframedescend%3D1&uin=40529743&ouin=3008868552&flag=100&key=3008868552&msgTime=1624433913&type=3&strmsg=iii%E6%B2%A1%E6%9C%89
     * @param $qq
     * @param $refuseqq
     * @param string $msg
     * @return array|string
     */
    public static function refuseFriendCall($qq, $refuseqq, $msg = '') {
        $pm['url'] = 'https://h5.qzone.qq.com/proxy/domain/w.qzone.qq.com/cgi-bin/tfriend/procfriendreq.cgi?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Frc.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fv8%2Fpages%2Ffriends%2Ffriend_msg_setting.html%3Fmode%3Drefuse%26ouin%3D3008868552%26id%3D1%26flag%3D100%26key%3D3008868552%26time%3D1624433913%26g_iframeUser%3D1%26g_iframedescend%3D1&uin='.$qq.'&ouin='.$refuseqq.'&flag=100&key='.$refuseqq.'&msgTime='.time().'&type=3&strmsg='.urlencode($msg);
        parse_str($poststr, $posts);
        $pm['post'] = $posts;
        $pm['referer'] = 'https://rc.qzone.qq.com/myhome/friends/center';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 同意好友请求
     * @param $qq
     * @return array
     */
    public static function agreeFriendCall($qq, $agreeqq) {
        $pm['url'] = 'https://rc.qzone.qq.com/proxy/domain/w.qzone.qq.com/cgi-bin/tfriend/procfriendreq.cgi?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Frc.qzone.qq.com%2Fmyhome%2Ffriends%2Fcenter&uin='.$qq.'&ouin='.$agreeqq.'&flag=100&key='.$agreeqq.'&msgTime='.time().'&type=1&groupId=0&from_source=11';
        parse_str($poststr, $post);
        $pm['post'] = $post;
        $pm['referer'] = 'https://rc.qzone.qq.com/myhome/friends/center';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 添加可能认识人好友
     * qzreferrer=https%3A%2F%2Frc.qzone.qq.com%2Fmyhome%2Ffriends%2Fcenter&sid=0&ouin=84128086&uin=40529743&fuin=84128086&sourceId=1&fupdate=1
     * @param $qq
     * @param $addqq
     * @return array
     */
    public static function addMayFriend($qq, $addqq) {
        $pm['url'] = 'https://rc.qzone.qq.com/proxy/domain/w.qzone.qq.com/cgi-bin/tfriend/friend_authfriend.cgi?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Frc.qzone.qq.com%2Fmyhome%2Ffriends%2Fcenter&sid=0&ouin='.$addqq.'&uin='.$qq.'&fuin='.$addqq.'&sourceId=1&fupdate=1';
        parse_str($poststr, $post);
        $pm['post'] = $post;
        $pm['referer'] = 'https://rc.qzone.qq.com/myhome/friends/center';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 可能认识的人，陌生人非好友
     * @param $qq
     * @return array
     */
    public static function myMayFriendList($qq) {
        $pm['url'] = 'https://rc.qzone.qq.com/proxy/domain/r.qzone.qq.com/cgi-bin/potential/potentialpy_homepage.cgi?uin='.$qq.'&page=1&num=50&rd=0.'.microtime(true).'&fupdate=1&g_tk=';
        $pm['referer'] = 'https://rc.qzone.qq.com/myhome/friends/center';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 好友请求信息列表
     * @param $qq
     * @return array
     */
    public static function myFriendRequestList($qq) {
        $pm['url'] = 'https://rc.qzone.qq.com/proxy/domain/r.qzone.qq.com/cgi-bin/tfriend/getfriendmsglist.cgi?uin='.$qq.'&fupdate=1&rd='.time().'&version=8&g_tk=';
        $pm['referer'] = 'https://rc.qzone.qq.com/myhome/friends/center';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        return $msg;
    }

    /**
     * 我的QQ群
     * @param $qq
     * @param int $appid
     * @return array
     */
    public static function myqqGroups($qq, $appid = 100679599) {
        $pm['url'] = 'https://apphub.qzone.qq.com/fusion/cgi-bin/qzapps/appinvite_qqlist.cgi?appid='.$appid.'&uin='.$qq.'&rd=0.'.microtime(true).'&g_tk=';
        $pm['referer'] = 'https://apphub.qzone.qq.com/proxy/domain/qzs.qzone.qq.com/open/dialog/invite/invite.htm';
        $ret = static::getInfos($qq, $pm);
        $msg = static::responseMsg($ret);
        $arr = $msg[0];
        unset($msg[0]);

        $msg['qqgroups'] = [];
        if (!empty($arr['grouplist'])) {
            foreach ($arr['grouplist'] as $v) {
                $msg['qqgroups'][$v['group_code'].''] = [
                    'groupid' => $v['group_code'],
                    'name' => $v['group_name'],
                ];
            }
        }
        return $msg;
    }

    /**
     * 将好友移到指定分组并设置备注
     * @param $qq
     * @param $toqq
     * @param $togid
     * @param $tonick
     * @return array
     */
    public static function moveFriendToGroup($qq, $toqq, $togid, $tonick) {
        $pm['url'] = 'https://h5.qzone.qq.com/proxy/domain/w.qzone.qq.com/cgi-bin/tfriend/friend_chggroupid.cgi?g_tk=';
        $poststr = 'qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fv8%2Fpages%2Ffriends%2Ffriend_msg_setting.html%3Fmode%3Dpass1%26ouin%3D35039296%26id%3D0%26flag%3D102%26key%3D0%26time%3D%26g_iframeUser%3D1%26g_iframedescend%3D1&gpid='.$togid.'&ifuin='.$toqq.'&uin='.$qq.'&flag=102&key=0&rd=0.'.microtime(true).'&remark='.urlencode($tonick).'&fupdate=1';
        parse_str($poststr, $post);
        $pm['post'] = $post;
        $pm['referer'] = 'https://user.qzone.qq.com/'.$qq.'/';
        $ret = static::getInfos($qq, $pm);
        return static::responseMsg($ret);
    }

    /**
     * 未实名无法注册空间游戏应用APP
     * @param $qq
     * @param int $appid
     * @return array
     */
    public static function regapp($qq, $appid = 362) {
        $param['url'] = 'https://appsupport.qq.com/cgi-bin/qzapps/userapp_addapp.cgi?uin='.$qq.'&g_tk=';
        $param['post']= array(
            'appid' => $appid,
            'detail' => '',
            'g_tk' => static::qzone_gtk($qq),
            'qzreferrer' => 'https://my.qzone.qq.com/app/347.html?via=appcenter',//开心大富翁游戏
            //'qzreferrer' => 'https://rc.qzone.qq.com/362?via=appcenter&fixapp=1',//Q宠大乐斗
            'uin' => $qq,
            'via' => 'appcenter',
        );//echo $appid;
        $ret = static::getInfos($qq, $param);//echo $ret;exit;//echo strstr($datas, '"openid":"');
        $arr = static::jsonFormatArr($ret);
        return Lev::responseMsg(1, $ret, [$arr]);
    }

    /**
     * 修改QQ号昵称
     *
     * $pm['post'] = 'qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fv6%2Fsetting%2Fprofile%2Fprofile.html%3Ftab%3Dbase%26g_iframeUser%3D1'
     * .'&nickname='.urlencode($nick)
     * .'&emoji=&sex=1&birthday=1990-09-14&province=50&city=12&country=1&marriage=0&bloodtype=1&hp=50&hc=23&hco=1&career=%E8%AE%A1%E7%AE%97%E6%9C%BA%2F%E4%BA%92%E8%81%94%E7%BD%91%2FIT&company=&cp=0&cc=0&cb=&cco=0&lover=&islunar=0&mb=1&uin='.$qq.'&pageindex=1&fupdate=1';
     *https://h5.qzone.qq.com/proxy/domain/w.qzone.qq.com/cgi-bin/user/cgi_apply_updateuserinfo_new?g_tk=1038220003
     * @param $qq
     * @param $nick
     * @return array
     */
    public static function editMyNick($qq, $nick) {
        $pm['url'] = 'https://h5.qzone.qq.com/proxy/domain/w.qzone.qq.com/cgi-bin/user/cgi_apply_updateuserinfo_new?g_tk=';
        $pm['post'] = 'qzreferrer=https%3A%2F%2Fuser.qzone.qq.com%2Fproxy%2Fdomain%2Fqzs.qq.com%2Fqzone%2Fv6%2Fsetting%2Fprofile%2Fprofile.html%3Ftab%3Dbase%26g_iframeUser%3D1'
            .'&nickname='.urlencode($nick)
            .'&emoji=&cp=0&cc=0&cb=&cco=0&lover=&islunar=0&mb=1&uin='.$qq.'&pageindex=1&fupdate=1';
        parse_str($pm['post'], $post);
        $pm['post'] = $post;
        $pm['referer'] = 'https://user.qzone.qq.com/'.$qq.'/';
        $ret = static::getInfos($qq, $pm);
        return static::responseMsg($ret);
    }

    /**
     * 获取QQ号空间开放openid 过期
     * @param $qq
     * @return array
     */
    public static function getQQopenid($qq) {echo $qq;
        $param['url'] = 'https://appframe.qq.com/cgi-bin/qzapps/appframe.cgi?appid=362&qz_ver=6&pf=qzone&via=QZ.MYAPP&v=1&g_tk=';
        $param['referer'] = 'https://user.qzone.qq.com/'.$qq.'/myhome/362?via=QZ.MYAPP';
        $ret = static::getInfos($qq, $param);//echo $ret;
//        $datas = str_replace(array('_Callback(', ');'), '', $ret);
//        $datas = json_decode(trim($datas), true);
        $datas['res'] = static::jsonFormatArr($ret);
        $datas['ret'] = $ret;//echo '<pre>';print_r($datas);exit();
        return Lev::responseMsg(1, '', $datas);
    }

    /**
     * 添加QQ好友 过期
     * @param $qq
     * @param $addqq
     * @return array
     */
    public static function addfriend($qq, $addqq) {
        $open = static::getQQopenid($addqq);
        if (empty($open['data']['openid'])) {
            return Lev::responseMsg(-800100, '抱歉，QQopenid获取失败');
        }
        $openid = $open['data']['openid'];
        $arr['url'] = 'https://w.cnc.qzone.qq.com/cgi-bin/tfriend/friend_addfriend.cgi?g_tk=';
        $arr['post'] = array(
            'qzreferrer'=>urlencode('http://qzs.qzone.qq.com/qzone/v6/friend_manage/addfriend/index.html#openid='.$openid.'&v=88&_fd_id=dialog_2&_fd_c=3893986407&appid=362&platform=qzone&quiet=1'),
            'client_id'=>'362',
            'sid'=>'500',
            'ouin'=> $openid,
            'from_source'=>'',
            'uin'=> $qq,
            'fupdate'=>'1',
            'rd'=>'0.'.rand(100000, 999999).Lev::$app['timestamp'],
            'flag'=>'0',
            'groupId'=>'0',
            'chat'=>'',
            'key'=>'',
            'im'=>'0',
            'g_tk'=>static::qzone_gtk($qq),
        );
        $ret = static::getInfos($qq, $arr);
//        $jsn = strstr($ret, 'frameElement.callback(');
//        $str = explode(');', $jsn);
//        $res = str_replace('frameElement.callback(', '', $str[0]);
//        $res = json_decode($res, true);//echo '<pre>'; print_r($res);
        //$datas['res'] = static::jsonFormatArr($ret);
        //$res['ret'] = $ret;
        //return Lev::responseMsg(1, '', $res);
        return static::responseMsg($ret);
    }

    public static function myFriend($qq) {
        $pm['url'] = 'https://user.qzone.qq.com/proxy/domain/r.qzone.qq.com/cgi-bin/tfriend/friend_ship_manager.cgi?uin='.$qq.'&do=1&rd=0.'.microtime(true).'&fupdate=1&clean=1&g_tk='.static::qzone_gtk($qq).'';
        $pm['referer'] = 'https://user.qzone.qq.com/'.$qq.'/';
        $ret = static::getInfos($qq, $pm, 0);
        $datas['res'] = static::jsonFormatArr($ret);
        //$datas['str'] = $str;
        //$datas['ret'] = $ret;
        return $datas;
    }

    //好友列表 + 好友分组
    //http://r.qzone.qq.com/cgi-bin/tfriend/friend_mngfrd_get.cgi?uin=675049572&rd=0.5184435830451548&g_tk=1313807328&fupdate=1
    /**
     * @param $qq
     * @param int $debug
     * @return array
     */
    public static function myGroupFriend($qq) {
        $param['url'] = 'https://r.qzone.qq.com/cgi-bin/tfriend/friend_mngfrd_get.cgi?uin='.$qq.'&rd=0.518443'.Lev::$app['timestamp'].'&fupdate=1&g_tk=';
        $param['referer'] = 'https://ctc.qzs.qq.com/qzone/v6/friend_manage/index.html';
        $ret = static::getInfos($qq, $param);
        $msg = static::responseMsg($ret);
        $arr = $msg[0];

        //$result['ret'] = $ret;
        $result['hynum'] = 0;
        $result['lists'] = [];
        $result['groups'] = [];
        $result['qqstatus'] = $msg['status'] >0 ? 0 : $msg;
        if (!empty($arr['data']['items'])) {
            foreach ($arr['data']['gpnames'] as $v) {
                $result['groups'][$v['gpid']] = [
                    'hynum' => 0,
                    'name' => $v['gpname'],
                    'groupid' => $v['gpid'],
                ];
            }
            foreach ($arr['data']['items'] as $v) {
                $_hy['qq'] = $v['uin'].'';
                $_hy['nick'] = $v['name'];
                $_hy['realname'] = $v['realname'];//备注
                $_hy['sex'] = $v['gender'] ? 0 : 1;//1：男性，0：女性
                $_hy['avatar'] = $v['img'];
                $_hy['groupid'] = $v['groupid'];
                $_hy['gname'] = $result['groups'][$_hy['groupid']]['name'];
                $result['groups'][$_hy['groupid']]['hynum'] += 1;
                $result['lists'][$_hy['qq']] = $_hy;
            }
            $result['hynum'] = count($result['lists']);
        }

        return $result;
    }

}