<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-10 12:14
 *
 * 项目：levs  -  $  - login2021.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


include __DIR__ . '/login_form.php';
?>

<script>
    !function () {
        window.onerror = function (n, e, o) {
            var t = document.createElement("img"),
                _ = encodeURIComponent(n + "|_|" + e + "|_|" + o + "|_|" + window.navigator.userAgent);
            //t.src = ''//"//ui.ptlogin2.qq.com/cgi-bin/report?id=195279&msg=" + _ + "&v=" + Math.random()
        }
    }();
    var g_cdn_js_fail = !1, pt = {};
    pt.str = {
        no_uin: "你还没有输入帐号！",
        no_pwd: "你还没有输入密码！",
        no_vcode: "你还没有输入验证码！",
        inv_uin: "请输入正确的帐号！",
        inv_vcode: "请输入完整的验证码！",
        qlogin_expire: "你所选择号码对应的QQ已经失效，请检查该号码对应的QQ是否已经被关闭。",
        other_login: "帐号登录",
        h_pt_login: "帐号密码登录",
        otherqq_login: "QQ帐号密码登录",
        onekey_return: "返回扫码登录"
    }, pt.ptui = {
        s_url: "https\x3A\x2F\x2Fminigame.qq.com\x2Fother\x2Floginproxy.html\x3Frefresh\x3D1",
        proxy_url: "https\x3A\x2F\x2Fqqgameplatcdn.qq.com\x2Fsocial_hall\x2Fapp_frame\x2Fptlogin_proxy.html",
        jumpname: encodeURIComponent(""),
        mibao_css: encodeURIComponent(""),
        defaultUin: "",
        lockuin: parseInt("0"),
        href: "https\x3A\x2F\x2Fxui.ptlogin2.qq.com\x2Fcgi-bin\x2Fxlogin\x3Fdaid\x3D207\x26target\x3Dself\x26appid\x3D7000201\x26s_url\x3Dhttps\x253A\x2F\x2Fminigame.qq.com\x2Fother\x2Floginproxy.html\x253Frefresh\x253D1\x26style\x3D22\x26proxy_url\x3Dhttps\x253A\x2F\x2Fqqgameplatcdn.qq.com\x2Fsocial_hall\x2Fapp_frame\x2Fptlogin_proxy.html",
        login_sig: "",
        clientip: "",
        serverip: "",
        version: "202112011710",
        ptui_version: encodeURIComponent("21120117"),
        isHttps: !0,
        cssPath: "https://ui.ptlogin2.qq.com/style.ssl/40",
        domain: encodeURIComponent("qq.com"),
        fromStyle: parseInt(""),
        pt_3rd_aid: encodeURIComponent("0"),
        appid: encodeURIComponent("7000201"),
        lang: encodeURIComponent("2052"),
        style: encodeURIComponent("40"),
        low_login: encodeURIComponent("0"),
        daid: encodeURIComponent("207"),
        regmaster: encodeURIComponent(""),
        enable_qlogin: "1",
        noAuth: "0",
        target: isNaN(parseInt("0")) ? {_top: 1, _self: 0, _parent: 2}["0"] : parseInt("0"),
        csimc: encodeURIComponent("0"),
        csnum: encodeURIComponent("0"),
        authid: encodeURIComponent("0"),
        auth_mode: encodeURIComponent("0"),
        pt_qzone_sig: "0",
        pt_light: "0",
        pt_vcode_v1: "1",
        pt_ver_md5: "000D64FF6AF2E4247B21E209EB22A1DBCF002087B988CCCCD4B51233",
        gzipEnable: "1"
    };
</script>
