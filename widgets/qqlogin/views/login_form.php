<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 15:27
 *
 * 项目：levs  -  $  - login_form.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');
?>


<style>
    .login_form.list-block .submit-btn.item-inner::after {height: 0}
    .login_form.list-block .item-input input {height: 43px;}
</style>

<div class=" flex-box ju-sa" style="width:100%">
    <div class="login_form list-block card no-hairlines" style="background: #fefefe;max-width:100%;min-width: 320px;padding-right:15px;">
        <?php if (!empty($qqs)):?>
            <div class="item-content">
                <div class="item-media">
                    <svg class="icon"><use xlink:href="#fa-qq"></use></svg>
                </div>
                <div class="item-inner">
                    <div class="item-input">
                        <select class="loginedQQ" style="font-size:12px" autocomplete="off">
                            <option value="">已登陆QQ</option>
                            <?php foreach ($qqs as $v):?>
                                <option value="<?=$v['qq']?>" pwd="<?=$v['qqpwd']?>"><?=$v['qq'],$v['qqnick']?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <form id="loginform" name="loginform" action="" method="post" target="0" style="margin:0px;">
            <div class="item-content">
                <div class="item-media">
                    <svg class="icon"><use xlink:href="#fa-qqhy"></use></svg>
                </div>
                <div class="item-inner uinArea" id="uinArea">
                    <div class="hiddenx">
                        <label class="input_tips" id="uin_tips" for="u" data-onlyqq="QQ号码" style="display: none;">支持QQ号/邮箱/手机号登录</label>
                    </div>
                    <div class="item-input">
                        <div class="inputOuter">
                            <input type="text" class="inputstyle" placeholder="请输入QQ号码" id="u" name="u" value="" tabindex="1">
                            <a class="uin_del" id="uin_del" href="javascript:void(0);" style="display: block;"></a>
                        </div>
                    </div>
                    <ul class="email_list" id="email_list" style="display: none;"></ul>
                </div>
            </div>
            <div class="item-content">
                <div class="item-media">
                    <svg class="icon"><use xlink:href="#fa-safe"></use></svg>
                </div>
                <div class="item-inner pwdArea" id="pwdArea">
                    <div class="hiddenx">
                        <label class="input_tips" id="pwd_tips" for="p" style="display: block;">密码</label>
                    </div>
                    <div class="item-input">
                        <div class="inputOuter">
                            <input type="password" class="inputstyle password" placeholder="请输入QQ密码" id="p" name="p" value="" maxlength="16" tabindex="2">
                        </div>
                    </div>
                    <div class="hiddenx">
                        <div class="lock_tips" id="caps_lock_tips" style="display: none;">
                            <span class="lock_tips_row"></span> <span>大写锁定已打开</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-content">
                <div class="item-media">
                    <svg class="icon"><use xlink:href="#fa-info"></use></svg>
                </div>
                <div class="item-inner verifyArea" id="verifyArea">
                    <div class="verifyinputArea" id="verifyinputArea">
                        <div class="hiddenx">
                            <label class="input_tips" id="vc_tips" for="verifycode">验证码</label>
                        </div>
                        <div class="item-input">
                            <div class="inputOuter">
                                <input name="verifycode" autocomplete="off" type="text" placeholder="验证码" class="inputstyle verifycode" id="verifycode" value="" tabindex="3">
                            </div>
                        </div>
                    </div>
                    <div class="hiddenx">
                        <div class="verifyimgArea item-after" id="verifyimgArea">
                            <img class="verifyimg" id="verifyimg" title="看不清，换一张">
                            <a tabindex="4" href="javascript:void(0);" class="verifyimg_tips date">看不清，换一张</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-content">
                <div class="item-inner submit-btn" style="padding: 0 15px;margin: 10px 0 10px 10px;">
                    <div class="submit item-input buttons-row" style="width: 100%">
                        <a class="login_button flex-box ju-sa" href="javascript:void(0);" hidefocus="true" style="width: 100%;justify-content: center">
                            <?php if ($logintypes):?>
                            <select onchange="window.location=this.value" class="button button-fill color-orange wd100" style="font-size: 17px;" autocomplete="off">
                                <?php foreach ($logintypes as $_type => $v):?>
                                    <option value="<?=\modules\qq\helpers\UrlQqHelper::qqlogin($_type)?>" <?=$_type == $logintype ? 'selected' : ''?>><?=$v?></option>
                                <?php endforeach;?>
                            </select>
                            <?php endif;?>
                            <input type="submit" tabindex="6" value="登 录" class="btn button-fill button color-lightblue" id="login_button" style="font-size: 17px;">
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    jQuery(function () {
        qqJs.qq = '<?=$qq?>';
        qqJs.pwd = '<?=$pwd?>';
    });
</script>

