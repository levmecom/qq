<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 14:19
 *
 * 项目：levs  -  $  - qzone.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


include __DIR__ . '/login_form.php';
?>

<script> !function () {
        window.onerror = function (n, e, o) {
            var t = document.createElement("img"),
                _ = encodeURIComponent(n + "|_|" + e + "|_|" + o + "|_|" + window.navigator.userAgent);
            //t.src = "//ui.ptlogin2.qq.com/cgi-bin/report?id=195279&msg=" + _ + "&v=" + Math.random()
        }
    }();
    var g_cdn_js_fail = !1, pt = {};
    pt.str = {
        no_uin: "你还没有输入帐号！",
        no_pwd: "你还没有输入密码！",
        no_vcode: "你还没有输入验证码！",
        inv_uin: "请输入正确的帐号！",
        inv_vcode: "请输入完整的验证码！",
        qlogin_expire: "你所选择号码对应的QQ已经失效，请检查该号码对应的QQ是否已经被关闭。",
        other_login: "帐号登录",
        h_pt_login: "帐号密码登录",
        otherqq_login: "QQ帐号密码登录",
        onekey_return: "返回扫码登录"
    }, pt.ptui = {
        s_url: "https\x3A\x2F\x2Fuser.qzone.qq.com\x2Fproxy\x2Fdomain\x2Fqzs.qq.com\x2Fac\x2Fqzone\x2Flogin\x2Fsucc.html\x23para\x3Dmall\x26",
        proxy_url: "https\x3A\x2F\x2Fqzs.qq.com\x2Fqzone\x2Fv6\x2Fportal\x2Fproxy.html",
        jumpname: encodeURIComponent(""),
        mibao_css: encodeURIComponent(""),
        defaultUin: "",
        lockuin: parseInt("0"),
        href: "https\x3A\x2F\x2Fxui.ptlogin2.qq.com\x2Fcgi-bin\x2Fxlogin\x3Fdaid\x3D5\x26ptredirect\x3D1\x26proxy_url\x3Dhttps\x253A\x2F\x2Fqzs.qq.com\x2Fqzone\x2Fv6\x2Fportal\x2Fproxy.html\x26pt_no_auth\x3D1\x26hide_title_bar\x3D1\x26low_login\x3D0\x26qlogin_auto_login\x3D1\x26no_verifyimg\x3D1\x26link_target\x3Dblank\x26appid\x3D549000912\x26style\x3D22\x26target\x3Dself\x26pt_qr_app\x3D\x25E6\x2589\x258B\x25E6\x259C\x25BAQQ\x25E7\x25A9\x25BA\x25E9\x2597\x25B4\x26pt_qr_link\x3Dhttp\x253A\x2F\x2Fz.qzone.com\x2Fdownload.html\x26self_regurl\x3Dhttps\x253A\x2F\x2Fqzs.qq.com\x2Fqzone\x2Fv6\x2Freg\x2Findex.html\x26pt_qr_help_link\x3Dhttp\x253A\x2F\x2Fz.qzone.com\x2Fdownload.htmlappid\x3D15004501\x26target\x3Dself\x26f_url\x3Dhttps\x253A\x252F\x252Fqzs.qq.com\x252Fac\x252Fqzone\x252Flogin\x252Ferror.html\x26s_url\x3Dhttps\x253A\x252F\x252Fuser.qzone.qq.com\x252Fproxy\x252Fdomain\x252Fqzs.qq.com\x252Fac\x252Fqzone\x252Flogin\x252Fsucc.html\x2523para\x253Dmall\x2526",
        login_sig: "",
        clientip: "",
        serverip: "",
        version: "202112101237",
        ptui_version: encodeURIComponent("21121012"),
        isHttps: !0,
        cssPath: "https://ui.ptlogin2.qq.com/style.ssl/40",
        domain: encodeURIComponent("qq.com"),
        fromStyle: parseInt(""),
        pt_3rd_aid: encodeURIComponent("0"),
        appid: encodeURIComponent("549000912"),
        lang: encodeURIComponent("2052"),
        style: encodeURIComponent("40"),
        low_login: encodeURIComponent("0"),
        daid: encodeURIComponent("5"),
        regmaster: encodeURIComponent(""),
        enable_qlogin: "1",
        noAuth: "1",
        target: isNaN(parseInt("0")) ? {_top: 1, _self: 0, _parent: 2}["0"] : parseInt("0"),
        csimc: encodeURIComponent("0"),
        csnum: encodeURIComponent("0"),
        authid: encodeURIComponent("0"),
        auth_mode: encodeURIComponent("0"),
        pt_qzone_sig: "0",
        pt_light: "0",
        pt_vcode_v1: "1",
        pt_ver_md5: "000D64FF6AF2E4247B21E209EB22A1DBCF002087B988CCCCD4B51233",
        gzipEnable: "1"
    }; </script>
