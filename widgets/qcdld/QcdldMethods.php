<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 01:51
 *
 * 项目：levs  -  $  - ${FILE_NAME}
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\widgets\qcdld;

use modules\qq\widgets\qzone\qzoneHelper;

class QcdldMethods extends QcdldBases
{

    //注册大乐斗
    public static function registerLD($qq) {
        //$arr['url'] = 'http://appsupport.qq.com/cgi-bin/qzapps/userapp_addapp.cgi?uin='.$qq.'&g_tk=74444604';
        $sex = rand(102, 105);//随机103女QMM--102男QGG
        if ($sex !=102) $sex = 103;
        $name = 'QGG';   if ($sex ==103)   $name = 'QMM';

        $url = 'https://fight.pet.qq.com/cgi-bin/petpk?cmd=regist&sex='.$sex;//103女QMM--102男QGG

        $msg = static::getInfos($url, $qq);
        if ($msg['result'] == '0') {
            static::showMsg('成功领养一只' . $name . '，注册大乐斗成功！' . $msg['msg'], 1);
        }
        $msg = qzoneHelper::regapp($qq, 362);
        static::$msgs .= $msg['message'];
        //static::newhand($qq);
    }

    public static function newhand($qq) {
        for ($i=100; $i<108; $i++) {
            $url = 'https://fight.pet.qq.com/cgi-bin/petpk?cmd=newhand&type=1&taskid='.$i;//新人指引奖
            $msg = static::getInfos($url, $qq);
            static::showMsg($msg['msg'], 6);
            if (strpos($msg['msg'], '级以上') !==FALSE) break;
        }
    }
}