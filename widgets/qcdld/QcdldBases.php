<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 01:51
 *
 * 项目：levs  -  $  - ${FILE_NAME}
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\widgets\qcdld;

use Lev;
use lev\helpers\curlHelper;
use modules\qq\helpers\qqLoginHelper;

class QcdldBases
{

    public static $qq = '';
    public static $cookiefiles = [];

    public static $msgs = '';

    public static function showMsg ($msg, $type = 0, $color = 'blue') {
        switch ($type) {
            case 1 : static::$msgs .= '<span style="color:#cc0000;">'.$msg.'</span>';
                break;
            case 2 : static::$msgs .= '挑战 &nbsp;<span style="color:green">'.$msg.' 成功！</span>';
                break;
            case 3 : static::$msgs .= '挑战 &nbsp;<span style="color:red">'.$msg.' 失败！</span>';
                break;
            case 4 :
                $_msg = strstr($msg, 'Win');
                if (!$_msg) {
                    $array = static::jsonDecode($msg);
                    static::showMsg($array['msg']);
                    static::ckLoginError($array['msg'], true);
                    return $array['msg'];
                }else {
                    static::showMsg($_msg, 5);
                }
                break;
            case 5 :
                $strARR = array('Win:0', 'Win:1', '&Exp:', '&yueli:', '&contribute:');
                $strREP = array('<span style="color:green;">胜利！ </span>', '<span style="color:red;">失败！ </span>', ' 经验：', ' 阅历：', ' 帮贡：');
                $msg = substr($msg, 0, 50);
                $_msg = str_replace($strARR, $strREP, $msg);
                static::$msgs .= $_msg.'';
                break;
            case 6 :
                static::$msgs .= '<span style="color:'.$color.';">'.$msg.'</span>';
                break;
            case 7 :
                static::$msgs .= '<span style="color:blue;">'.$msg.'</span>';
                break;
            case 10 :
                static::$msgs .= '<span style="color:#F60;">'.$msg.'</span>';
                break;
            default:
                static::$msgs .= $msg;
                break;
        }
        if ($type != 99) {
            static::$msgs .= '<br />';
        }
        return FALSE;
    }

    //获取信息，返回一个数组
    public static function getInfos($url, $qq, $cklogin = 1, $cookie = '', $param = array()) {
        if (!$url) return 'nourl';
        $param['url'] = $url;//获取信息的URL地址

        if (!isset(static::$cookiefiles[$qq])) {
            static::$cookiefiles[$qq] = qqLoginHelper::createCK($qq);
        }
        $param['cookiefile'] = static::$cookiefiles[$qq];

        if (!$param['referer']) {
            $param['referer'] = 'https://fight.pet.qq.com/fightindex.html?sourceid=108&ADTAG=cop.innercop.qqsh-actionhall';
        }
        if ($cookie) {
            if ($cookie ==2) {//大厅登陆
                $cookie = ' ts_refer=minigame.qq.com/plat/social_hall/app_frame/; ts_last=fight.pet.qq.com/vip.html; EXPIRES=Fri, 02-Jan-2020 00:00:00 GMT; PATH=/; DOMAIN=qq.com; wherefrom=4; EXPIRES=Fri, 02-Jan-2020 00:00:00 GMT; PATH=/; DOMAIN=qq.com;';
            }
            $param['cookie'] .= ' '.$cookie;
        }//print_r($param);
        $jsons = curlHelper::doCurl($param);//print_r($jsons);
        if ($cookie && strpos($cookie, 'xxpetid') !==FALSE) {
            $jsons = static::jsonDecode($jsons);
        }else {
            $jsons  = iconv('GBK', 'UTF-8//IGNORE', $jsons);
            $_jsons = str_replace("'", '', $jsons);
            $jsons  = static::jsonDecode($_jsons);//print_r($jsons);exit;
        }

        if ($cklogin) {
            $jsons['qqstatus'] = static::ckLoginError($jsons['msg']);
        }

        return $jsons;
    }

    public static function ckLoginError($need, $exit = false, $msg = ' &raquo; <a href="javascript:;" onclick="QQLogin_id()" class="redb">重新登陆</a>...') {
        if (strpos($need, '登陆校验失败') !==FALSE) {
            $tips = '-5'.$need.$msg;
            if ($exit) {
                static::showMsg($tips, -5);
                exit();
            }
            return Lev::responseMsg(-5, $tips);
        }
        return false;
    }

    public static function jsonDecode($json) {
        return json_decode($json, true);
    }
    public static function _jsonDecode($json) {
        if (empty($json)) return array();
        $comment = false;
        $x = '';
        $out = '$x=';

        for ($i=0; $i<strlen($json); $i++) {
            if (!$comment) {
                if (($json[$i] == '{') || ($json[$i] == '[')) {
                    $out .= ' array(';
                }else if (($json[$i] == '}') || ($json[$i] == ']')) {
                    $out .= ')';
                }else if ($json[$i] == ':') {
                    $out .= '=>';
                }else {
                    $out .= $json[$i];
                }
            }else {
                $out .= $json[$i];
            }
            if ($json[$i] == '"' && $json[($i-1)]!="\\")    $comment = !$comment;
        }
        eval($out . ';');
        return $x;
    }

}