<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-09 21:38
 *
 * 项目：levs  -  $  - qcdldGameHelper.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\widgets\qcdld;

use Lev;
use lev\helpers\curlHelper;
use modules\qq\helpers\qqLoginHelper;

class qcdldGameHelper extends QcdldMethods
{
    /**
     * 我的好友
     * @param $qq
     */
    public static function myFriend($qq) {
        $url = 'https://fight.pet.qq.com/cgi-bin/petpk?cmd=view&kind=1&sub=1&selfuin='.$qq;
        $msg = self::getInfos($url, $qq);//print_r($msg);

        $result['hynum'] = 0;
        $result['lists'] = [];
        $result['qqstatus'] = Lev::arrv('qqstatus', $msg, 0);
        if (!empty($msg['info'])) {
            $infos = array_slice($msg['info'], 7);
            foreach ($infos as $v) {
                if ($v['uin'] >1000) {
                    $_hy['qq'] = $v['uin'].'';
                    $_hy['nick'] = $v['name'];
                    $_hy['sex'] = $v['sex'];//1：男性，0：女性
                    $_hy['avatar'] = '//qlogo4.store.qq.com/qzone/'.$v['uin'].'/'.$v['uin'].'/100';
                    $result['lists'][$_hy['qq']] = $_hy;
                }
            }
            $result['hynum'] = count($result['lists']);
        }
        return $result;
    }

    public static function dailyaward($qq) {//连续登陆奖励
        //if (self::TaskMsg(132, '连续登陆奖励')) return TRUE;
        $url = 'https://fight.pet.qq.com/cgi-bin/petpk?cmd=award&op=1&type=0';
        $msg = self::getInfos($url, $qq);print_r($msg);
        if (strpos($msg['msg'], '您还未注册') !==FALSE) {
            Lev::showMessages($msg['msg'], $msg['result']);
            //self::registerLD();
        }else {
            Lev::showMessages($msg['ContinueLogin'].' '.$msg['DailyAward'].$msg['msg'], $msg['result']);
            //if (strpos($msg['msg'], '已经') !==FALSE || ($msg['ContinueLogin'] && $msg['DailyAward'])) {
            //	self::update_task(self::$qq, 132);
            //	return TRUE;
            //}
        }
    }

}