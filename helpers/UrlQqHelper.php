<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-10 11:37
 *
 * 项目：levs  -  $  - UrlQqHelper.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\helpers;

use Lev;

class UrlQqHelper extends UrlQzoneHelper
{

    public static function addfriend($qq, $addqq = null) {
        return Lev::toReWrRoute(['qzone/add-friend', 'qq'=>$qq, 'addqq'=>$addqq]);
    }

    public static function myqqgroup($qq) {
        return Lev::toReRoute(['qzone/myqq-groups', 'qq'=>$qq]);
    }

    public static function qqlogin($logintype = null) {
        return Lev::toReWrRoute(['qq-login', 'opid'=>$logintype]);
    }

    public static function myfriend($qq, $force = null) {
        return Lev::toReWrRoute(['qq/my-friend', 'qq'=>$qq, 'force'=>$force]);
    }

    public static function saveSendCookieUrl()
    {
        return Lev::toReRoute(['qq-login/save-send-cookie-url']);
    }

    public static function getSendCookieUrl($uid, $suid = 0, $state = '') {
        $link = qquserCacheHelper::mySendCookieUrl($uid);
        return $link.(strpos($link, '?') ? '&' : '?').'suid='.$suid.'&state='.$state;
    }

    public static function share($uid = null, $state = '', $suid = '')
    {
        $uid === null &&
        $uid = Lev::$app['uid'] <1 ? null : Lev::$app['uid'];
        $link = Lev::toReWrRoute(['qq-login/share', 'opid'=> $uid]);
        return $link.(strpos($link, '?') ? '&' : '?').'suid=0&state=';
    }

    public static function sendQqUrl()
    {
        return Lev::toReWrRoute(['qq-login/send-qqurl']);
    }

    public static function sendgift($qq, $sendqq)
    {
        return Lev::toReRoute(['qzone/send-gift', 'qq'=>$qq, 'sendqq'=>$sendqq]);
    }
}