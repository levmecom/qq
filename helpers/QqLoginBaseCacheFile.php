<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 12:13
 *
 * 项目：levs  -  $  - ${FILE_NAME}
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\helpers;

use Lev;
use lev\helpers\cacheFileHelpers;
use lev\helpers\UserCacheHelper;

class QqLoginBaseCacheFile extends cacheFileHelpers
{

    public static $cacheDir = '/qq-login';

    public static $fileExt = '.txt';

    public static function updateHynum($qq, $hynum) {
        if ($qq && $hynum >0) {
            static::setQqdata($qq, 'hynum', $hynum);
        }
    }
    public static function updateMyFriend($qq, $friendlists) {
        if ($qq && $friendlists) {
            static::setQqdata($qq, null, $friendlists, 'myFriend');
        }
    }
    public static function getMyFriend($qq) {
        return static::setQqdata($qq, null, null, 'myFriend');
    }

    static public function getQqdatas($qq) {
        return static::getQqdata($qq);
    }
    static public function getQqdata($qq, $key = null) {
        return static::setQqdata($qq, $key);
    }
    static public function setQqdata($qq, $key = null, $value = null, $file = 'qqdata') {
        $ckey = static::getQQCacheKey($qq, $file);
        !is_array($result = static::getc($ckey, false)) && $result = [];
        if ($value === null) {
            return $key === null ? $result : Lev::arrv($key, $result, '');
        }
        if ($key === null) {
            $result = $value;
        }else {
            $result[$key] = $value;
        }
        return static::setc($ckey, $result);
    }

    public static function getQQCacheKey($qq, $uid = null) {
        $uid === null &&
        $uid = Lev::$app['uid'];
        return UserCacheHelper::getUserCacheKey($qq, $uid);
    }

    static public function createCKsig($qq) {
        return static::createCK($qq.'.2');
    }

    /*
     * 创建cookie文件
     * 文件名为 $qq
     */
    static public function createCK($qq) {
        $ckey = static::getQQCacheKey($qq);
        $cookiefile = static::filec($ckey);
        if (!$cookiefile) {
            static::setc($ckey, '');
            $cookiefile = static::filec($ckey);
        }
        return $cookiefile;
    }


}