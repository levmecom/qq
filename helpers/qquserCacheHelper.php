<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-10 11:42
 *
 * 项目：levs  -  $  - userCacheHelper.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\helpers;

use Lev;
use lev\helpers\UserCacheHelper;

class qquserCacheHelper extends UserCacheHelper
{

    public static function mySendCookieUrl($uid, $link = null) {
        $key = 'mySendCookieUrl';
        if ($link === null) {
            return UserCacheHelper::getUserSetting($uid, $key);
        }
        return UserCacheHelper::setUserSetting($uid, $key, $link);
    }

}