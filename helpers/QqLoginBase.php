<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-11 12:11
 *
 * 项目：levs  -  $  - QqLoginBase.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\qq\helpers;

use Lev;
use lev\base\Assetsv;
use lev\base\Requestv;
use lev\helpers\curlHelper;
use lev\helpers\ModulesHelper;
use lev\helpers\UrlHelper;
use modules\levqq\levqqHelper;
use modules\qq\qqHelper;
use modules\qq\table\qq\qqModelHelper;

class QqLoginBase extends QqLoginBaseCacheFile
{

    /**
     * WEBQQ 新版登陆加密函数 失效
     *
     * @access public
     * @param string $pt
     * @param string $p
     * @param string $vc
     * @return string
     */
    public static function jspassword($pt,$p,$vc){
        return strtoupper(md5(strtoupper(md5(md5($p,true).$pt).strtoupper($vc))));
    }

    /*
     * 将cookie转换成数组
     * $key cookie名称
     * 传入$key时返回对应值
     * 默认以数组形式返回所有cookie
     *
     */

    public static function cookieArr($arr = array()) {
        return cookieHelper::cookieArrv($arr[0], $arr[1], $arr[2]);
    }

    //将文本式cookie转换成数组
    public static function ckfArr($arr = array()) {
        return cookieHelper::ckfArrv($arr[0], $arr[1], $arr[2]);
    }

    public static function setCloginUrl($loginUrl, $loginPm) {
        $arr = explode('?', $loginUrl, 2);
        parse_str($arr[1], $result);
        $loginPm += (array)$result;
        $url = $arr[0] . '?';
        foreach ($loginPm as $k => $v) {
            $k == 'u1' && $v = urlencode(urldecode($v));
            $url .= $k.'='.$v.'&';
        }
        return $url;
    }

    public static function loginIp() {
        $ip = Requestv::getRemoteIP();
        (!$ip || strpos($ip, '192.168.') ===0 || strpos($ip, '127.0.0.') === 0) && $ip = '27.12.185.142';
        return $ip;
    }

    //登录验证；通过 $arr['url'] 获得验证码 $data['code'] and $data['cookiefile']

    /**https://ssl.ptlogin2.qq.com/login?verifycode=!A5Q&psalt=2&ptdrvs=ilfTVIRZRclW1e1QJ4Z3OHKZsPjvNLWeC8J2ByAtoXUC4nJYanqIlE4ZiQqg4cw5&sid=8063280528367941312&pt_verifysession_v1=943229d6d538b7f0a71dc6ce8d8873bbae438724df3df59931ee6c9aa98c981c44bf343e0a2b44394b2adf82c3aa2d77&u=345555&pt_vcode_v1=0&p=z8h*fYfkXZV1LI4l2iaRB8tIjoY1bE-7t7rYsuQD*KE0MOrxrS1W-gfTzdbdo-hEvM*R13Tqbt43JMWUv*T6YL5PTsGpLBitTgPJT2wQ*ZJD*7zlpQdcawW1YnVKseH0wA2W*8bz92tJokd657iv8dqS0RB5qqqiLk7xbH-d3fU7CFmO4Rw5Qofm*em8Ea8fyWy-wne9Rj5uYy7EAMtuZjXSicCM5e96zBTGp-ItLlfR4sC2*sQHkHiSieY*Hx7WQh2mRlVWmkYms4gsuV0ah6WX2jcBOJABr1tcwa-QUw537tUJFoA-oeXvD6H1nlRZgCyEVWdRAlNfaVRUp99Nxg__&pt_randsalt=0&u1=https://minigame.qq.com/other/loginproxy.html?refresh=1&ptredirect=0&h=1&t=1&g=1&from_ui=1&ptlang=2052&action=1-14-1639052995785&js_ver=21120117&js_type=1&login_sig=&pt_uistyle=40&aid=7000201&daid=207
     *
     * https://ssl.ptlogin2.qq.com/login?verifycode=!PLW&pt_randsalt=2&ptdrvs=TBfGbziVGvUCgeEVcErnR3xyC5yL2v6Se88zuutIeiMpFlB*n8Am7vp7UW7KmqkE&sid=4398753803662548685&pt_verifysession_v1=72cff17d31aaa4605e01dac979e28efec6658a0cd11fc810b8d55cdb7c1ab0f878b768cadcb0f9ecec510bdbbc2a0d7e5eba8b73896188b5&u=40529743&pt_vcode_v1=0&p=NkQnI8FrinD8a-1M1McXnkndp1qR*R-rwAkiboNsmq1T*CEPVo4kchKiIsntW0JyKD24TrioVftc*hbsNO9Tbv1vnHL6b6OTS-rh16gsN9Z6tv4tb8RfnyoD4x-300GEDJdpBGPQFRdjOTNrOdnTylq7SkfPYNKvz5nwf0wI1m*2UhjzzESaLlzETiDxtaDqgX1FiSXwzpJkbOmXajCv9wLm8DyrAzQ2gymXBtyttczA7Uj8WFlFCzSGL*3-L*vTek0Hm7*8lAZUDHRxd43d20dkGnbvw5yTo-wVeIj2G86F885lyP7-41-S9X3-si*qSdqIz80VCfXlw6oc0z2CIg__&u1=https://user.qzone.qq.com/proxy/domain/qzs.qq.com/ac/qzone/login/succ.html#para=mall&&ptredirect=0&h=1&t=1&g=1&from_ui=1&ptlang=2052&action=1-0-1639210702966&js_ver=21121012&js_type=1&login_sig=&pt_uistyle=40&aid=549000912&daid=5&
     * @param $qq
     * @param string $checkUrl
     * @return array
     */
    static public function check($qq, $pwd, $checkUrl = '') {
        //登录验证地址（关键） 随时有可能失效；
        if (!$qq) return [];
        //$arr['url'] = "http://check.ptlogin2.qq.com/check?uin={$qq}&appid=1003903&r=0.33354983771036073";
        //$arr['url'] = "https://ssl.ptlogin2.qq.com/check?pt_tea=1&uin={$qq}&appid=501004106&js_ver=10114&js_type=0&login_sig=PQHYLzZUbtd9JXqUdPVilrBSmabbbqMjlNrQqUZIR-PUbvHKS-DhjC-x5HFBAAZ6&u1=http%3A%2F%2Fw.qq.com%2Fproxy.html&r=0.5768296722147539";
        $arr['url'] = "https://ssl.ptlogin2.qq.com/check?pt_tea=1&uin={$qq}&appid=501004106&js_ver=10117&js_type=0&login_sig=R8KlNGlEt810Y8i9YoWxAdsXVfHSSQvjC1l26TPipRWUklP3DoNP6d2PsZeuR1ZD&u1=http%3A%2F%2Fw.qq.com%2Fproxy.html&r=0.96049958".Lev::$app['timestamp'];
        //$arr['url'] = 'https://ssl.ptlogin2.qq.com/check?regmaster=&pt_tea=2&pt_vcode=1&uin='.$qq.'&appid=21000501&js_ver=21120117&js_type=1&login_sig=u-BmgxrBGyHIpwZOZhLyFRfANA3XAbvc7FnERmh4Y0LTf0ShYTN-xMc4tUaGDNF5&u1=https%3A%2F%2Ffight.qq.com%2Fcomm-htdocs%2Flogin%2FloginSuccess.html%3Fs_url%3Dhttps%253A%252F%252Ffight.qq.com%252F&r=0.0010000629807914718&pt_uistyle=40';
        //$arr['url'] = 'https://ssl.ptlogin2.qq.com/check?regmaster=&pt_tea=2&pt_vcode=1&uin='.$qq.'&appid=7000201&js_ver=21120117&js_type=1&login_sig=gzlw*xtSgBUu76Giv0uY1exlfCOuZ-S63aBWVWFPm*-UoaWpkZ6gB2okQ*ynT078&u1=https%3A%2F%2Fminigame.qq.com%2Fother%2Floginproxy.html%3Frefresh%3D1&r=0.5548351203606863&pt_uistyle=40';

        $checkUrl && $arr['url'] = $checkUrl;

        $cookiefile = static::createCK($qq);
        $arr['cookiejar'] =  $cookiefile;//创建验证时保存cookie的文件；
        //$arr['cookie'] = '	pgv_pvi=7472035840; pt2gguin=o0040678743; RK=NA8GEAulQ0; ptcz=7098564da138a258c7829701c10c769084859298d46293df60ffb5a865af5e6f; pgv_pvid=8073863010; o_cookie=40678743; luin=o0040678743; lskey=0001000038e4469a54861462d516011bbfffc006d3cf8c456a01c95b92074b5f1dca77ec5228f805242f90ff; ptui_loginuin=1204586054; pgv_si=s6527212544; uikey=fe0d4857f18a3a3b85f54cd9dc71ea8db49fc287acbb229d83a83b6798add218; qrsig=AEygL0cI-BC7tPscemvTwo5OHfIUY4qjD-XGSy8pqiz*ibOIDmr8vbdys*CFEDk1; ETK=; ptisp=cnc; ptuserinfo=e798a7e990b92de784a1; u_40678743=@gDHocAswW:1405334605:1405334605:e798a7e990b92de784a1:1; qqmusic_uin=0040678743; qqmusic_key=@gDHocAswW; qqmusic_fromtag=6; pgv_info=ssid=s2647444271; _qz_referrer=user.qzone.qq.com; confirmuin=0; verifysession=h011b703a4c6fe07c92048159604bed49ea0098b934c32cad31471e9b459a8dd12f320e67faff53752808d3869b20ddbf6b';
        $arr['cookie'] = '	pt2gguin=o0040678743; ptcz=7098564da138a258c7829701c10c769084859298d46293df60ffb5a865af5e6f; pgv_pvid=8073863010; o_cookie=40678743; lv_irt_id=f2418a8b375c75a0b9d92db4292bfe76; pgv_pvi=1842170629; RK=AJ8GQAuMU0; ptui_loginuin=2268913732%20; luin=o0040678743; lskey=0001000007dfccce9a487640fd7b220af62069053c3e4172cad916ea4e0fa062be8ab945c4c483b3eed74a0e; pgv_info=ssid=s6498966200&ssi=s2921853731; uikey=69e58dde335fdfaab07e6ce8e8d39e438ce89c03a9ecaa6d15f03fe4a01ab282; pt_local_token=-1424532947; qrsig=2u-5I3Cxz9qXUyXD3YlQHM6osBEqKTHGbK6OlCAcfEgGtyVBoo8c14OHf07AKwrO; confirmuin=2268913732; ptvfsession=47f8f507e36b7935991a914040a46478d1035572f46f6def76ada0ac792d366f947eca6b3923c78b289974b200f567fb; ptisp=cnc; clientuin=40678743; ETK=; supertoken=2508880203; ptuserinfo=e798a7e990b92de784a1; ptnick_40678743=e798a7e990b92de784a1; u_40678743=@a7NgtgE18:1425465058:1425465058:e798a7e990b92de784a1:1; qqmusic_uin=0040678743; qqmusic_key=@pwsQBuUMb; qqmusic_fromtag=6; verifysession=h02R9Qq5EoMX6GLIFXS8wts-6Tu51FyPEr9WXHHOVefwxDak3u07NJfhSOo2xuyOOiPTQNV62D2SCjwq15fEOYdDg**; ptnick_3203075851=3230313530323037313238; u_3203075851=@DAbeR5XZu:1425457905:1425457905:3230313530323037313238:0; superuin=o0040678743; superkey=0l94W-Z518UudBxqEL5lTfbQ9cOVhKKd6R4wWwZ8BRE_; chkuin=2268913732; dlock=5_1425456294_1_; uin=o0040678743; ptnick_2268913732=32303131303630382d303138; u_2268913732=@m9EAnsBf2:1425463625:1425463625:32303131303630382d303138:0';

        $arr['ip'] = static::loginIp();

        $check = curlHelper::doCurl($arr);

        //处理返回的数据；
        $datas = $check ? explode("','", strstr(explode("('", $check, 2)[1], "')", true)) : [];
        /*
                第一个参数是验证方式，叫ptVcodeV1

                第二个参数是验证码verifyCode

                第三个参数是uin，其实就是输入的QQ号的十六进制编码

                第四个参数是ptVerifysessionV1，也是用来验证的

                第五个参数是密码加密的盐ptRandSalt

                第六个参数是ptdrvs

                第七个参数是sid
        */
        $data['check'] = $check;
        $data['vcode'] = $datas[1];
        $data['capid'] = $datas[1];
        $data['ptuin'] = $datas[2];//substr(strstr($check, "\\x"), 0, -3);
        $data['checkUrl'] = $arr['url'];
        $data['pmarr'] = $datas;
        $data['loginPm'] = Lev::base64_encode_url(json_encode([
            'verifycode' => $datas[1],
            'pt_randsalt'=> $datas[4],
            'ptdrvs'     => $datas[5],
            'sid'        => $datas[6],
            'pt_verifysession_v1' => $datas[3],
        ]));
        $data['cookiefile'] = $cookiefile;
        $pwd = qqModelHelper::ckQQpwd($pwd);
        $data['pwd'] = $pwd;
        qqModelHelper::saveQQ($qq, $pwd, $check, '', 1);
        return $data;
    }

    public static function cloginUrl($qq, $pwd, $loginUrl, $loginPm, $checkSig = true) {
        $loginUrl = static::setCloginUrl($loginUrl, $loginPm);
        return static::clogin($qq, $pwd, $loginUrl, $checkSig);
    }

    //通过 $data['code'] and $data['cookiefile'] 执行登陆
    static public function clogin($qq, $pwd, $loginUrl, $checkSig = true) {
        //if (!$user || !$password || !$check) return 'dddd';

        $cookiefile = static::createCK($qq);
        //腾讯webqq登录地址，与微博等其它登录地址有区别。（随时都有可能失效！）
        //$arr['url'] = "http://ptlogin2.qq.com/login?u={$qq}&p={$password}&verifycode={$check}&webqq_type=10&remember_uin=1&login2qq=0&aid=1003903&u1=http%3A%2F%2Fweb.qq.com%2Floginproxy.html%3Flogin2qq%3D0%26webqq_type%3D10&h=1&ptredirect=0&ptlang=2052&from_ui=1&pttype=1&dumy=&fp=loginerroralert&action=7-34-678494&mibao_css=m_webqq&t=2&g=1";

        //$arr['url'] = "https://ssl.ptlogin2.qq.com/login?u={$qq}&p={$password}&verifycode={$check}&webqq_type=10&remember_uin=1&login2qq=1&aid=1003903&u1=http%3A%2F%2Fweb2.qq.com%2Floginproxy.html%3Flogin2qq%3D1%26webqq_type%3D10&h=1&ptredirect=0&ptlang=2052&daid=164&from_ui=1&pttype=1&dumy=&fp=loginerroralert&action=3-14-26236&mibao_css=m_webqq&t=1&g=1&js_type=0&js_ver=10034&login_sig=mTkTgX--OPJymUc7t-CPyoKwuWvIz-Ttbfnb15uze4Ihk-h96d-wwKuJ07jFfEuj";
        //$arr['url'] = 'https://ssl.ptlogin2.qq.com/login?u='.$qq.'&verifycode='.$check.'&pt_vcode_v1=0&pt_verifysession_v1='.$vers.'&p='.$password.'&pt_randsalt=2&u1=https%3A%2F%2Ffight.qq.com%2Fcomm-htdocs%2Flogin%2FloginSuccess.html%3Fs_url%3Dhttps%253A%252F%252Ffight.qq.com%252F&ptredirect=0&h=1&t=1&g=1&from_ui=1&ptlang=2052&action=10-19-1638971838747&js_ver=21120117&js_type=1&login_sig=u-BmgxrBGyHIpwZOZhLyFRfANA3XAbvc7FnERmh4Y0LTf0ShYTN-xMc4tUaGDNF5&pt_uistyle=40&aid=21000501&daid=8&ptdrvs=*cc3NKwK*hrnwm0fyd2AHzLgC0fQrOwNwS8JWfEKCFk3ThwWCrYd5kl5MJfxQ3BU&sid=3814927006352587120&';
        //$arr['url'] = 'https://ssl.ptlogin2.qq.com/login?u='.$qq.'&verifycode='.$check.'&pt_vcode_v1=0&pt_verifysession_v1='.$vers.'&p='.$password.'&pt_randsalt=2&u1=https%3A%2F%2Fminigame.qq.com%2Fother%2Floginproxy.html%3Frefresh%3D1&ptredirect=0&h=1&t=1&g=1&from_ui=1&ptlang=2052&action=6-10-1638974276578&js_ver=21120117&js_type=1&login_sig=gzlw*xtSgBUu76Giv0uY1exlfCOuZ-S63aBWVWFPm*-UoaWpkZ6gB2okQ*ynT078&pt_uistyle=40&aid=7000201&daid=207&ptdrvs='.$ptdrvs.'&sid='.$sid.'&';

        $arr['url'] = $loginUrl;

        $arr['ip'] = static::loginIp();

        $arr['httpheader'] = ['Host: ssl.ptlogin2.qq.com
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:80.0) Gecko/20100101 Firefox/80.0
Accept: */*
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Referer: https://xui.ptlogin2.qq.com/
Sec-Fetch-Dest: script
Sec-Fetch-Mode: no-cors
Sec-Fetch-Site: same-site'];
        $arr['cookiefile'] = $cookiefile; //curl check()的cookiefile 必须；
        $arr['cookiejar'] = $cookiefile; //创建保存登录后cookie的文件；
        //$arr['header'] =  TRUE;//输出头部信息
        $ret = curlHelper::doCurl($arr);
        //$arr['dbcookie2'] = static::saveCookie($ret, $qq);

        //处理返回的数据；ptuiCB
        $datas = $ret ? explode("','", strstr(explode("('", $ret, 2)[1], "')", true)) : [];

        $result['login'] = $ret;
        $result['pmarr'] = $datas;
        $result['loginCookie'] = file_get_contents($cookiefile);
        $result['loginMsg'] = $datas[4];
        $result['qqnick'] = isset($datas[5]) ? $datas[5] : explode("', '", $datas[4])[1];
        empty($datas[0]) && strpos($datas[4], '成功') !== false && $result['loginStatus'] = 0;
        $pwd = qqModelHelper::ckQQpwd($pwd);
        $qqstatus = isset($result['loginStatus']) ? 2 : 3;
        qqModelHelper::saveQQ($qq, $pwd, $ret, $result['qqnick'], $qqstatus);
        if (isset($result['loginStatus'])) {
            if ($checkSig && UrlHelper::check($datas[2])) {
                $loginCookie = static::checkSig($qq, $datas[2]);//二次登陆
                $result['loginCookie'] = $loginCookie ."\r\n" . $result['loginCookie'];
            }

            if (ModulesHelper::isInstallModule('levqq')) {
                levqqHelper::setQqLogin($qq, '', $result['loginCookie'], $result['qqnick']);
            }
        }
        Lev::$app['isAdmin'] && $result['curlpm'] = $arr;
        return $result;

    }

    /**
     * https://ptlogin2.qzone.qq.com/check_sig?pttype=1&uin=40529743&service=login&nodirect=0&ptsigx=44e56ec2722a40f10c66d23717fd2d757f449dbb8db53e49a33f8ee8633ba5a119130e6e2f8df904495077c71140ee7526c3cbd867378aa2e7df031819099fb6&s_url=https%3A%2F%2Fqzs.qzone.qq.com%2Fqzone%2Fv5%2Floginsucc.html%3Fpara%3Dizone&f_url=&ptlang=2052&ptredirect=100&aid=549000912&daid=5&j_later=0&low_login_hour=0&regmaster=0&pt_login_type=1&pt_aid=0&pt_aaid=0&pt_light=0&pt_3rd_aid=0
     *
     * ptuiCB('0','0','https://ptlogin2.qzone.qq.com/check_sig?pttype=1&uin=40529743&service=login&nodirect=0&ptsigx=44e56ec2722a40f10c66d23717fd2d757f449dbb8db53e49a33f8ee8633ba5a119130e6e2f8df904495077c71140ee7526c3cbd867378aa2e7df031819099fb6&s_url=https%3A%2F%2Fqzs.qzone.qq.com%2Fqzone%2Fv5%2Floginsucc.html%3Fpara%3Dizone&f_url=&ptlang=2052&ptredirect=100&aid=549000912&daid=5&j_later=0&low_login_hour=0&regmaster=0&pt_login_type=1&pt_aid=0&pt_aaid=0&pt_light=0&pt_3rd_aid=0','0','登录成功！', '40529743')
     * @param $qq
     * @param $url
     * @return false|string
     */
    public static function checkSig($qq, $url)
    {

        $cookiefile = static::createCK($qq);
        $arr['url'] = $url;

        $arr['ip'] = static::loginIp();

        //$arr['cookie'] = 'pt_guid_sig=5360c7ddc027a7050f6092d8ac8e77af5cc41ffd5ff3c47d2a2bfde6b7b724bd; pt2gguin=o0227248948; pt_recent_uins=3c9ebf16587cf522edd8810859a1e37064df9c3697ad992f43f0742edfde1351c549c38d0665330bba550d6ace2670b8aa77d21f569a6937; RK=XJ4MFCueU2; ptcz=e54aada138c89406d423ba24171531fe73dfce46989fae039a888d20f10f659d; pgv_pvid=7770823172; tvfe_boss_uuid=491b1b79cea86726; sd_userid=9271625381944622; sd_cookie_crttime=1625381944622; Qs_lvt_323937=1625384289%2C1625384326%2C1625384340%2C1625384605; Qs_pv_323937=859974702060372700%2C2243670163251119000%2C1115062810482376800%2C2836588362373321700; _tc_unionid=0558f3d8-6176-45c2-aa1f-e8699cde4959; ptui_loginuin=40529743; ETK=; ptnick_227248948=e5b8aee5b8aee5b8aee7bd917ce9878de5ba86; confirmuin=0; ptdrvs=VQFramMZLueWYqW3zi4Tt1GU1ENy61g1hNeBQXND8*SQ-4FU2Hp6yjACUxbYutuF; ptvfsession=e51d19b9d27b15933adbc0f7385a89abdb9644edcceeed554fe2b52b2576a710a903a39435a41270dfff03fbfb2c39b6f7b1ad542a1a26c1; ied_qq=o0040529743; eas_sid=j1R6T3c8f9Y64944o1H0J301M4; pgv_info=pgvReferrer=&ssid=s3992709540; pt_login_sig=5*z9RXAzR31E4VYt7NIG5HlcLSJ6vyh42SRT*q9sHXxHqhF7tfizlN92AiECeHEr; pt_clientip=1fc51b0cb98e7102; pt_serverip=e7480991b2d2c0d8; pt_local_token=-1354461382; uikey=9c635a6f173d8e5f8c77fc10d2abec3e18ca23d953aedcf137467b7a4c095e98; qrsig=P9egxCSZBSG*SjIu-ORptTkrAxRLE9QiGjWeVU0KUQnO-f9wnHsTU47D58nBk28I; clientuin=227248948; gameapi_access_token=01000000e5a86470022947e4fae8cc67be4567d044d43d44f9e02910bb4c9ec3ee1b6bdc75758f99649192d7; qqgame_nick=%E5%B8%AE%E5%B8%AE%E5%B8%AE%E7%BD%91%7C%E9%87%8D%E5%BA%86%20; supertoken=571962429; ptnick_675049572=e5858be6a0bce58b83; o_cookie=675049572; rv2=80CEBF4BB03BA481D9056FAA971F43461B16C7B578F2794198; property20=6263FA1569AD326F55C59EE6B43BF995AE0571D0992221959B77285F2EEBDE83A129B02B39B7B14B; superuin=o0040529743; superkey=8vV9IkNp*S7evcX2GM8kUwXDiny8fvbjHzvWgSOQqqY_; ptnick_40529743=3430353239373433; qm_authimgs_id=2; qm_verifyimagesession=h019c98184e89414654e65d498ee86694111f7bdf4dc19dff0e1ad9ee38aad0fd29adc6d5b8eea6d9ee; logout_page=dm_loginpage; dm_login_weixin_rem=; IED_LOG_INFO2=userUin%3D40529743%26nickName%3D40529743%26userLoginTime%3D1639112637; dev_mid_sig=ef3f0ec43045ad8fda9ba09533c917149044bb6bb5950d2b4f82ae7c984c030178579dd28e647b27; _qz_referrer=i.qq.com';

        //能否获取到cookie 重要参数
        $host = parse_url($url, PHP_URL_HOST);
        $arr['httpheader'] = ['Host: '.$host.'
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:80.0) Gecko/20100101 Firefox/80.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Referer: https://xui.ptlogin2.qq.com/
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: iframe
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-site
Sec-Fetch-User: ?1'];
        $arr['cookiefile'] = $cookiefile; //curl check()的cookiefile 必须；
        $arr['cookiejar'] = $cookiefile;//static::createCKsig($qq); //创建保存登录后cookie的文件；
        $arr['header'] =  TRUE;//输出头部信息
        $ret = curlHelper::doCurl($arr);
        $loginCookie = file_get_contents($arr['cookiejar']);

        return $loginCookie;
    }

}